const dotenv = require("dotenv")

dotenv.config({
  path: `.env`,
})

module.exports = {
  // flags: {
  //   PRESERVE_WEBPACK_CACHE: true,
  //   DEV_SSR: true,
  //   FAST_DEV: true,
  //   PRESERVE_WEBPACK_CACHE: true,
  //   PRESERVE_FILE_DOWNLOAD_CACHE: true,
  //   PARALLEL_SOURCING: true,
  //   useNameForId: false,
  // },
  siteMetadata: {
    title: "uniform-optimize-gatsby-contentful-starter",
    description: "UniformConf, a Uniform Optimize demo site",
  },
  /* Your site config here */
  plugins: [
    "gatsby-plugin-postcss",
    "gatsby-plugin-react-helmet",
    {
      resolve: "gatsby-source-contentful",
      options: {
        spaceId: process.env.GATSBY_CONTENTFUL_SPACE_ID,
        accessToken:
          process.env.GATSBY_NODE_ENV === "production"
            ? process.env.GATSBY_CONTENTFUL_CDA_ACCESS_TOKEN
            : process.env.GATSBY_CONTENTFUL_CPA_ACCESS_TOKEN,
        host:
          process.env.GATSBY_NODE_ENV === "production"
            ? "cdn.contentful.com"
            : "preview.contentful.com",
        environment: process.env.GATSBY_CONTENTFUL_ENVIRONMENT
          ? process.env.GATSBY_CONTENTFUL_ENVIRONMENT
          : "master",
        downloadLocal:
          process.env.GATSBY_NODE_ENV === "production" ? false : true,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `GatsbyJS`,
        short_name: `GatsbyJS`,
        start_url: `/`,
        background_color: `#f7f0eb`,
        theme_color: `#a2466c`,
        display: `standalone`,
        icon: `static/images/logo.png`,
        icons: [
          {
            src: `static/images/logo.png`,
            sizes: `192x192`,
            type: `image/png`,
          },
          {
            src: `static/images/logo.png`,
            sizes: `512x512`,
            type: `image/png`,
          },
        ],
        legacy: true,
        purpose: "maskable",
      },
    },
    {
      resolve: `gatsby-plugin-offline`,
      // options: {
      //   precachePages: ["/", `/contact-us/`, `/destinations/*`, `/gallery/*`],
      //   importWorkboxFrom: `cdn`,
      //   appendScript: require.resolve(`./src/custom-sw-code.js`),
      // },
    },
    {
      resolve: `gatsby-plugin-sharp`,
      options: {
        defaults: {
          formats: [`auto`, `webp`],
          placeholder: `dominantColor`,
          quality: 50,
          breakpoints: [750, 1080, 1366, 1920],
          backgroundColor: `transparent`,
          tracedSVGOptions: {},
          blurredOptions: {},
          jpgOptions: {},
          pngOptions: {},
          webpOptions: {},
          avifOptions: {},
        },
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-image`,
    {
      resolve: "gatsby-plugin-load-script",
      options: {
        name: `pages`,
        src: `${__dirname}/static/`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `${__dirname}/static/`,
      },
    },
  ],
}
