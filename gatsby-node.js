const path = require("path")

// exports.onPostBuild = ({ reporter }) => {
// reporter.info("Your Gatsby site has been built!")
// }
// creatre contentful pages dynamically
exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions
  const pageTemplate = path.resolve("./src/templates/ContentfulPage.tsx")
  const galleryTemplate = path.resolve("./src/templates/DestinationGallery.tsx")
  const homeTemplate = path.resolve("./src/templates/HomePage.tsx")
  const contactPage = path.resolve("./src/templates/ContactUsPage.tsx")
  const destinationPage = path.resolve("./src/templates/Destination.tsx")

  const planAnEventPage = path.resolve("./src/templates/PlanAnEvent.tsx")
  const packagesPage = path.resolve("./src/templates/Packages.tsx")
  const quickPayPage = path.resolve("./src/templates/QuickPay.tsx")
  const TermsAndConditions = path.resolve("./src/templates/TermsAndConditions.tsx")
  const Testimonials = path.resolve("./src/templates/Testimonials.tsx")

  const meetingEventsPage = path.resolve("./src/templates/MeetingAndEvents.tsx")
  const partnerWithUs = path.resolve("./src/templates/PartnerWithUs.tsx")
  const BannerTest = path.resolve("./src/templates/BannerTest.tsx")
  const aboutus = path.resolve("./src/templates/AboutUs.tsx");
  const petPolicy = path.resolve("./src/templates/petPolicy.tsx")
  const careerPath = path.resolve("./src/templates/Career.tsx")
  const termsConditions = path.resolve("./src/templates/TermsConditions.tsx")
  const weddings = path.resolve("./src/templates/Weddings.tsx")


  const result = await graphql(`
    query {
      allContentfulPage {
        edges {
          node {
            id
            slug
            title
            sys {
              contentType {
                sys {
                  id
                  linkType
                  type
                }
              }
              revision
              type
            }
          }
        }
      }
    }
  `)
  // console.log("Loading....pages", JSON.stringify(result))

  result.data.allContentfulPage.edges.forEach(edge => {
    createPage({
      path: `${edge.node.slug}`,
      component: pageTemplate,
      context: {
        title: edge.node.title,
        slug: edge.node.slug,
        sys: {
          ...edge.node.sys,
        },
        components: edge.node.components, // array of components
      },
    })
  })
  //Gallery template
  const galleryResult = await graphql(`
    query gallery {
      allContentfulGallery {
        edges {
          node {
            title
            slug
            images {
              title
              file {
                url
              }
            }
            sys {
              contentType {
                sys {
                  id
                  linkType
                  type
                }
              }
              revision
              type
            }
          }
        }
      }
    }
  `)
  if (galleryResult.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }
  var galleryArr =
    galleryResult &&
    galleryResult.data &&
    galleryResult.data.allContentfulGallery &&
    galleryResult.data.allContentfulGallery.edges
  galleryArr.forEach(edge => {
    createPage({
      path: `/gallery/${edge.node.slug}`,
      component: galleryTemplate,
      context: {
        title: edge.node.title,
        slug: edge.node.slug,
        sys: {
          ...edge.node.sys,
        },
        components: edge.node.components,
      },
    })
  })

  // Home Page
  createPage({
    path: `/`,
    component: homeTemplate,
  })
  // Contact Page
  createPage({
    path: `/contact-us`,
    component: contactPage,
  })

  createPage({
    path: `/terms-conditions`,
    component: TermsAndConditions,
  })

  createPage({
    path: `/testimonials`,
    component: Testimonials,
  })

  // meeting and events page 
  createPage({
    path: `/meetings-events`,
    component: meetingEventsPage,
  })

  // partner with us page
  createPage({
    path: `/partner-with-us`,
    component: partnerWithUs,
  })

  // about us page
  createPage({
    path: `/about-us`,
    component: aboutus,
  })

  // pet-policy
  createPage({
    path: `/pet-policy`,
    component: petPolicy,
  })

  // career 
  createPage({
    path: `/career-us`,
    component: careerPath,
  })

  // TermsConditions
  // career 
  createPage({
    path: `/TermsCondition`,
    component: termsConditions,
  })

  // Weddings Page
  createPage({
    path: `/Weddings`,
    component: weddings,
  })



  // Destination Templates
  const destinationResult = await graphql(`
    query destinations {
      allContentfulDestination {
        edges {
          node {
            title
            slug
            sys {
              contentType {
                sys {
                  id
                  linkType
                  type
                }
              }
              revision
              type
            }
          }
        }
      }
    }
  `)
  if (destinationResult.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }
  //console.log("destinationResult", JSON.stringify(destinationResult))
  var desArr =
    destinationResult &&
    destinationResult.data &&
    destinationResult.data.allContentfulDestination &&
    destinationResult.data.allContentfulDestination.edges
  desArr.forEach(edge => {
    createPage({
      path: `/destinations/${edge.node.slug}`,
      component: destinationPage,
      context: {
        title: edge.node.title,
        slug: edge.node.slug,
        sys: {
          ...edge.node.sys,
        },
        components: edge.node.components,
      },
    })
  })

  //Packages page
  desArr.forEach(edge => {
    createPage({
      path: `/packages/${edge.node.slug}`,
      component: packagesPage,
      context: {
        title: edge.node.title,
        slug: edge.node.slug,
        sys: {
          ...edge.node.sys,
        },
        components: edge.node.components,
      },
    })
  })

  // Quickpay pages
  desArr.forEach(edge => {
    createPage({
      path: `/quick-pay/${edge.node.slug}`,
      component: quickPayPage,
      context: {
        title: edge.node.title,
        slug: edge.node.slug,
        sys: {
          ...edge.node.sys,
        },
        components: edge.node.components,
      },
    })
  })

  //pan an event pages
  desArr.forEach(edge => {
    createPage({
      path: `/plan-an-event/${edge.node.slug}`,
      component: planAnEventPage,
      context: {
        title: edge.node.title,
        slug: edge.node.slug,
        sys: {
          ...edge.node.sys,
        },
        components: edge.node.components,
      },
    })
  })
}

const intentTagResolver = (source, _args, context, _info) => {
  if (!source.unfrmOptIntentTag___NODE) {
    return null
  }

  const tagNode = context.nodeModel.getNodeById({
    id: source.unfrmOptIntentTag___NODE,
  })

  if (tagNode) {
    const { internal } = tagNode
    const { content } = internal

    if (content) {
      try {
        const json = JSON.parse(content)
        return json
      } catch (e) {
        console.error(e)
      }
    }
  }

  return null
}

const createUniformResolvers = ({ createResolvers, tagTypes }) => {
  createResolvers(
    tagTypes.reduce((p, c) => {
      p[c] = {
        intentTag: {
          type: "JSON",
          resolve: intentTagResolver,
        },
      }
      return p
    }, {})
  )
}

// Types to add "intentTag" fields to.
const taggedGraphQLTypes = ["ContentfulHero"]

exports.createResolvers = ({ createResolvers }) => {
  createUniformResolvers({
    createResolvers,
    tagTypes: taggedGraphQLTypes,
  })
}
