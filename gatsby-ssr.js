/**
 * Implement Gatsby's SSR (Server Side Rendering) APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/ssr-apis/
 */

// You can delete this file if you're not using it
const React = require("react")

export const onRenderBody = (
  { setPostBodyComponents, setHeadComponents },
  pluginOptions
) => {
  setPostBodyComponents([
    <script src="https://res.cloudinary.com/espire/raw/upload/v1620204580/samples/Js/jquery.min.js"></script>,
    <script src="https://res.cloudinary.com/espire/raw/upload/v1620204580/samples/Js/bootstrap.js"></script>,
    <script src="https://res.cloudinary.com/espire/raw/upload/v1620204580/samples/Js/jquery.fancybox.min.js"></script>,
    <script src="https://res.cloudinary.com/espire/raw/upload/v1620204872/samples/Js/slick.min.js"></script>,
    <script src="https://res.cloudinary.com/espire/raw/upload/v1621592686/samples/Js/owl.carousel.min.js"></script>,
    // <script src="https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"></script>,
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"></script>,
    <script src="https://www.eglobe-solutions.com/mailer/EglobeCalender/jquery-ui.js"></script>,
    
  ])
  setHeadComponents([])
}
