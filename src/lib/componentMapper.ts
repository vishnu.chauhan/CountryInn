import { createElement } from "react"
import { Hero } from "../components/Hero"
import { Experience } from "../components/Pages/Experience"

export const Components = {
  hero: Hero,
  experience:Experience
}

export const componentMapper = contentfulPage => {
  return contentfulPage.components.map((component, i) => {

    if (component?.sys?.contentType?.sys?.id) {      
      return createElement(
        Components[component.sys.contentType.sys.id] ?? (() => null),
        {
          key: i,
          ...component,
        }
      )
    }
  })
}
