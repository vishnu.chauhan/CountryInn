import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const PetPolicyAllPolicies: React.FC<any> = props => {

    const { contentfulPetsPolicyPage } = props;

    return (

        <section className="container-xxl py-5 hotals-tabs term-condition">
            <div className="row">
                <div className="container">
                    <h2 className="yellow-text-dark tk-gautreaux mt-4 mt-lg-3">Pets Policy</h2>

                    <ul className="mt-3 text-dark">
                        {contentfulPetsPolicyPage.petsPolicyList.map((item, index) => (
                            <li> <i className="fa fa-check"></i>
                                {item}
                            </li>

                        ))}

                        <li> <i className="fa fa-check"></i>
                            {contentfulPetsPolicyPage.petWelcomePropertiesHeading}
                            <ol>

                                {contentfulPetsPolicyPage.welcomeList.map((item, index) => (
                                    <li key={index}>{item}</li>
                                ))}

                            </ol>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
    )
}