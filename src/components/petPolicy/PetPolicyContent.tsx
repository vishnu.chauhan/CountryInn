import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const PetPolicyContent: React.FC<any> = props => {

    const { contentfulPetsPolicyPage } = props;

    return (

        <section style={{ backgroundImage: "url('https://www.countryinn.in/images/resource/pets-policy-1.jpg')" }}
            className="container-xxl pet-policy position-relative hotel-location contact-our-address">
            <div className="container pt-5 pb-4">

                <div className="row">
                    <div className="col-md-6">
                        <figure className="image-box pr-lg-4">
                            <img src={`${contentfulPetsPolicyPage.petImage.file.url}`} />
                        </figure>
                    </div>
                    <div className="col-md-6">
                        <div className="pl-md-5">
                            <h1 className=" yellow-text tk-gautreaux">{contentfulPetsPolicyPage.petTitle}  <span
                                className="text-white d-block">{contentfulPetsPolicyPage.petSubtitle}</span></h1>

                            <div className="text-white mt-4">
                                <p>{contentfulPetsPolicyPage.petContentPara1.petContentPara1}</p>
                                <p>{contentfulPetsPolicyPage.petContentPara2.petContentPara2}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    )
}






