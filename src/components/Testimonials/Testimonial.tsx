import React, { Fragment, useEffect, useState } from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const Testimonial: React.FC<any> = props => {
  const [currentPage, setCurrentPage] = useState(1)
  const [maxItemsPerPage, setMaxItemsPerPage] = useState(8)
  const [selectedValues, setselectedValues] = useState({
    locationFilter: "All",
  })
  const [data, setdata] = useState(props.data["nodes"])

  console.log(props.data)

  const changePage = (direction: string) => {
    if (direction == "back") {
      setCurrentPage(currentPage - 1)
    } else if (direction == "next") {
      setCurrentPage(currentPage + 1)
    }
  }

  const OnLocationChange = e => {
    setselectedValues({
      ...selectedValues,
      [e.target.name]: e.target.value,
    })
  }

  useEffect(() => {
    let newData = []
    if (selectedValues.locationFilter === "All") {
      setdata(props.data["nodes"])
    } else {
      let tempData = props.data["nodes"].filter(
        x => x.clientCity.trim() === selectedValues.locationFilter
      )
      newData.push(tempData[0])
      setdata(newData)
    }
  }, [selectedValues.locationFilter])

  return (
    <Fragment>
      <div
        className="home-container container-xxl"
        style={{ backgroundImage: "url('./images/bg-home-bottom.png')" }}
      >
        <div className="row">
          <div className="container mt-5 mt-lg-5">
            <div className="row pt-lg-5 mb-4 pt-4">
              <div className="col-md-7">
                <h1 className="yellow-text tk-gautreaux">
                  All Member's
                  <span className="text-white">Reviews </span>
                </h1>
              </div>
              <div className="col-md-5 text-right">
                <label className="yellow-text">
                  {" "}
                  Filter By Location
                  <select
                    className="ml-3 location-select"
                    name="locationFilter"
                    value={selectedValues.locationFilter}
                    onChange={OnLocationChange}
                  >
                    <option value={"All"}>All</option>
                    <option value={"Amritsar"}>Amritsar</option>
                    <option value={"Bhimtal"}>Bhimtal</option>
                    <option value={"Budhan"}>Budhan</option>
                    <option value={"Corbett"}>Corbett</option>
                    <option value={"Haridwar"}>Haridwar</option>
                    <option value={"Vrindavan"}>Vrindavan</option>
                  </select>
                </label>
              </div>
            </div>
            <div className="row">
              <ul className="home-testimonial testimonials-page">
                {data
                  .slice(
                    currentPage * maxItemsPerPage - maxItemsPerPage,
                    currentPage * maxItemsPerPage
                  )
                  .map(x => (
                    <li>
                      <a href="">
                        <img
                          className="img-testimonial"
                          src={x.clientMedia.file.url}
                          alt={x.clientMedia.file.fileName}
                        />
                        <p>{x.clientReview.clientReview}</p>
                      </a>
                      <div className="d-flex">
                        <div className="mr-2">
                          <img
                            className="testimonial-thumb"
                            src={x.clientIconImg.file.url}
                            alt={x.clientIconImg.file.fileName}
                          />
                        </div>
                        <div>
                          <h6 className="text-uppercase text-white">
                            {x.clientName}
                          </h6>
                          <h6 className="text-white font-weight-light">
                            <small>
                              {x.clientCompany}-{x.clientCity}
                            </small>
                          </h6>
                        </div>
                      </div>
                    </li>
                  ))}
              </ul>
            </div>
            <div className="text-center d-flex justify-content-center testiminial-nav pt-lg-3 pb-5">
              <a
                onClick={() => changePage("back")}
                // disabled={currentPage <= 1}
                style={{
                  pointerEvents: currentPage <= 1 ? "none" : "auto",
                  opacity: currentPage <= 1 ? "0.8" : "",
                }}
              >
                {" "}
                <i className="fa fa-arrow-circle-o-left" aria-hidden="true"></i>
                <small> PREV</small>
              </a>
              <a
                onClick={() => changePage("next")}
                style={{
                  pointerEvents:
                    data.length < maxItemsPerPage ? "none" : "auto",
                  opacity: data.length < maxItemsPerPage ? "0.8" : "",
                }}
                // disabled={data.length < maxItemsPerPage}
              >
                {" "}
                <small> NEXT</small>
                <i
                  className="fa fa-arrow-circle-o-right"
                  aria-hidden="true"
                ></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  )
}
