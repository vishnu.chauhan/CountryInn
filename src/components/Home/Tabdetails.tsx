import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const Tabdetails: React.FC<any> = props => {
    return (
        <StaticQuery
            query={graphql`
            query tabsIconQuery {
                allContentfulHomeTab(limit: 3) {
                  edges {
                    node {
                      title
                      subTitle
                      slug
                      tabIconImage {
                        file {
                          fileName
                          url
                        }
                      }
                    }
                  }
                }
              }
      `}
            render={data => (
                <div className="row">
                    <ul className="nav bg-secondary w-100 d-flex justify-content-around nav-tabs">
                        {data.allContentfulHomeTab.edges.map((item, key) => (
                            <li className="nav-item" key={key}>
                                <a
                                    className={`${key == 0
                                        ? "nav-link container-fuild p-3 pt-lg-5 pb-lg-3 px-lg-4 active"
                                        : "nav-link container-fuild p-3 pt-lg-5 pb-lg-3 px-lg-4"
                                        }`}
                                    data-toggle="tab"
                                    href={`#${item.node.slug}`}
                                >
                                    <div className="d-flex row no-gutters">
                                        <div className="col-lg-3">
                                            <div className="circle-icon">
                                                <img
                                                    src={item.node.tabIconImage.file.url}
                                                    alt="img"
                                                />
                                            </div>
                                        </div>
                                        <div className="col-lg-9">
                                            <h4 style={{ background: "#a26b1d00" }}>
                                                {item.node.title}
                                            </h4>
                                            <p
                                                className="d-none d-md-block"
                                                style={{ background: "#a26b1d00" }}
                                            >
                                                {item.node.subTitle}
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        ))}
                    </ul>
                </div>

            )}
        />
    )
}