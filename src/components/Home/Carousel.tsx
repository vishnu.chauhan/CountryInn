import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const Carousel: React.FC<any> = props => {
  return (
    <StaticQuery
      query={graphql`
      
        query homeCaurosal {
          contentfulDestinationsCarousel(slug: { eq: "Home" }) {
            title
            subTitle
            slug
            name
            description {
              description
            }
            
          }
        }
      `}
      render={data => (

        <div className="row pt-lg-4">
          <div className="col-md-9">
            <h1 className="yellow-text tk-gautreaux">
              {data.contentfulDestinationsCarousel &&
                data.contentfulDestinationsCarousel.title}
              <span className="text-white">
                {data.contentfulDestinationsCarousel &&
                  data.contentfulDestinationsCarousel.subTitle}
              </span>
            </h1>
            <p className="text-uppercase font-weight-200 text-white">
              {data.contentfulDestinationsCarousel &&
                data.contentfulDestinationsCarousel.description &&
                data.contentfulDestinationsCarousel.description.description}
            </p>
          </div>
          {/* <div className="col-md-3">
            <a href="/" className="view-all-border pull-right mt-md-4">
              VIEW ALL
              </a>
          </div> */}
        </div>

      )}
    />
  )
}
