import React, { Fragment } from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const ClientSays: React.FC<any> = props => {
  return (
    <StaticQuery
      query={graphql`
        query ClientSays {
          allContentfulClientSays {
            nodes {
              clientCity
              clientCompany
              clientName
              clientReview {
                clientReview
              }
              clientIconImg {
                id
                file {
                  fileName
                  url
                }
                description
              }
              clientMedia {
                description
                file {
                  url
                  fileName
                }
              }
            }
          }
        }
      `}
      render={data => (
        <Fragment>
          <div className="row pt-lg-5 mb-4 pt-4">
            <div className="col-md-9">
              <h1 className="yellow-text tk-gautreaux">
                What Our
                <span className="text-white">Client Says </span>
              </h1>
            </div>
            <div className="col-md-3">
              <Link
                to={"/testimonials"}
                className="view-all-border pull-right mt-md-0"
              >
                VIEW ALL
              </Link>
            </div>
          </div>

          <div className="row">
            <ul className="owl-carousel home-testimonial home-owl-carousel">
              {data.allContentfulClientSays.nodes.map(x => (
                <li className="p-2">
                  <Link to={""}>
                    <img
                      className="img-testimonial"
                      src={x.clientMedia.file.url}
                      alt={x.clientMedia.file.fileName}
                    />
                    <p>{x.clientReview.clientReview}</p>
                  </Link>
                  <div className="d-flex">
                    <div className="mr-2">
                      <img
                        className="testimonial-thumb"
                        src={x.clientIconImg.file.url}
                        alt={x.clientIconImg.file.fileName}
                      />
                    </div>
                    <div>
                      <h6 className="text-uppercase text-white">
                        {x.clientName}
                      </h6>
                      <h6 className="text-white font-weight-light">
                        <small>
                          {x.clientCompany} - {x.clientCity}
                        </small>
                      </h6>
                    </div>
                  </div>
                </li>
              ))}
            </ul>
          </div>
        </Fragment>
      )}
    />
  )
}
