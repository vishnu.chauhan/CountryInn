import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const AboutImages: React.FC<any> = props => {
    return (
        <StaticQuery
            query={graphql`
        query AboutImages {
          contentfulAboutUs {
            images {
              file {
                url
                fileName
              }
            }
          }
        }
      `}
            render={data => (

                <div className="col-lg-6">
                    <ul className="d-flex">
                        {data.contentfulAboutUs.images.map(
                            (item, key) =>
                                key % 2 === 0 && (
                                    <li key={key} className={`${key == 2 ? "mt-lg-5" : ""}`}>
                                        <Link
                                            to={
                                                data.contentfulAboutUs.images &&
                                                data.contentfulAboutUs.images[key] &&
                                                data.contentfulAboutUs.images[key].file &&
                                                data.contentfulAboutUs.images[key].file.url
                                            }
                                            data-fancybox
                                            data-caption={
                                                data.contentfulAboutUs.images &&
                                                data.contentfulAboutUs.images[key] &&
                                                data.contentfulAboutUs.images[key].file &&
                                                data.contentfulAboutUs.images[key].file.description
                                            }
                                        >
                                            <img
                                                className="about-grid-img"
                                                src={
                                                    data.contentfulAboutUs.images &&
                                                    data.contentfulAboutUs.images[key] &&
                                                    data.contentfulAboutUs.images[key].file &&
                                                    data.contentfulAboutUs.images[key].file.url
                                                }
                                                alt="img"
                                            />
                                        </Link>

                                        <Link
                                            to={
                                                data.contentfulAboutUs.images &&
                                                data.contentfulAboutUs.images[key + 1] &&
                                                data.contentfulAboutUs.images[key + 1].file &&
                                                data.contentfulAboutUs.images[key + 1].file.url
                                            }
                                            data-fancybox
                                            data-caption={
                                                data.contentfulAboutUs.images &&
                                                data.contentfulAboutUs.images[key + 1] &&
                                                data.contentfulAboutUs.images[key + 1].file &&
                                                data.contentfulAboutUs.images[key + 1].file
                                                    .description
                                            }
                                        >
                                            <img
                                                className="about-grid-img"
                                                src={
                                                    data.contentfulAboutUs.images &&
                                                    data.contentfulAboutUs.images[key + 1] &&
                                                    data.contentfulAboutUs.images[key + 1].file &&
                                                    data.contentfulAboutUs.images[key + 1].file.url
                                                }
                                                alt="img"
                                            />
                                        </Link>
                                    </li>
                                )
                        )}
                    </ul>
                </div>
            )}
        />
    )
}