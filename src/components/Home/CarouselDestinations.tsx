import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const CarouselDestinations: React.FC<any> = props => {
    return (
        <StaticQuery
            query={graphql`
        query CarouselDestinations {
            contentfulDestinationsCarousel(slug: { eq: "Home" }) {
                
                destination {
                  bookNowLink  
                  title
                  subTitle
                  slug
                  image {
                    title
                    file {
                      url
                    }
                  }
                }
              }
        }
      `}
            render={data => (

                <div className="row">
                    <ul className="owl-carousel home-owl-carousel">
                        {data.contentfulDestinationsCarousel &&
                            data.contentfulDestinationsCarousel.destination.map(
                                (item, key) => (
                                    <li className="p-2" key={key}>
                                        <Link to="">
                                            <img
                                                className="imgsize"
                                                src={`${item.image && item.image.file && item.image.file.url
                                                    }`}
                                                alt="img"
                                            />
                                            <div className="content">
                                                <h5 className="text-uppercase mb-0 text-white">
                                                    {item.title}
                                                </h5>
                                                <p className="mb-0 font-weight-200 text-white">
                                                    {item.subTitle}
                                                </p>
                                                <Link to={item.bookNowLink} className="book-button">
                                                    <span className="d-inline-block py-2 pl-3 pr-2">
                                                        BOOK NOW
                                                    </span>
                                                    <span className="d-inline-block p-2 bg-yellow-light">
                                                        <i
                                                            className="fa fa-angle-double-right"
                                                            aria-hidden="true"
                                                        ></i>
                                                    </span>
                                                </Link>
                                            </div>
                                        </Link>
                                    </li>
                                )
                            )}
                    </ul>
                </div>
            )}
        />
    )
}