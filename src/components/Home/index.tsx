import React from "react"
import { Link, graphql } from "gatsby"
import { Tabs } from "./Tabs"
import { Tabdetails } from "./Tabdetails"
import { About } from "./About"
import { Carousel } from "./Carousel"
import { ClientSays } from "./ClientSays"
import { CarouselDestinations } from "./CarouselDestinations"
import { AboutImages } from "./AboutImages";


export const Home: React.FC<any> = ({ }) => {
  return (
    <>
      <div className="container-xxl home-tabs">
        <>
          <Tabs />
          <Tabdetails />
        </>

      </div>
      <div
        className="home-container container-xxl"
        style={{
          background: "url('images/bg-home-bottom.png')",
        }}
      >
        <div className="row">
          <div className="container mt-5 mt-lg-5">
            <>
              <Carousel />
              <CarouselDestinations />
            </>
            <div className="container mt-5 mt-lg-5">
              <div className="row  home-about-section mt-5 pt-lg-4">
                <About />
                <AboutImages />
                
              </div>
              
            </div>
            <ClientSays/>
          </div>
        </div>
      </div>

    </>
  )
}
