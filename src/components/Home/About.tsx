import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"
export const About: React.FC<any> = props => {
  return (
    <StaticQuery
      query={graphql`
        query About {
          contentfulAboutUs {
            title
            thirdText
            description {
              description
            }
            thirdIcon {
              file {
                url
                fileName
              }
            }
            subTitle
            secondText
            secondIcon {
              file {
                fileName
                url
              }
            }
           
            firstText
            firstIcon {
              file {
                url
              }
            }
          }
        }
      `}
      render={data => (

        <div className="col-lg-6">
          <h1 className="yellow-text tk-gautreaux">
            {data.contentfulAboutUs.title}
            <span className="text-white">
              {data.contentfulAboutUs.subTitle}
            </span>
          </h1>
          <p className="font-weight-200 text-white">
            {data.contentfulAboutUs.description.description}
          </p>
          <div className="d-flex mt-4 home-about-3-icons justify-content-between">
            <div className="position-relative">
              <img
                src={data.contentfulAboutUs.firstIcon.file.url}
                alt="img"
              />
              <span className="line d-none d-md-block "></span>
              <p className="yellow-text my-2">
                <small>{data.contentfulAboutUs.firstText}</small>
              </p>
            </div>
            <div className="position-relative">
              <img
                src={data.contentfulAboutUs.secondIcon.file.url}
                alt="img"
              />
              <span className="line d-none d-md-block "></span>
              <p className="yellow-text my-2">
                <small>{data.contentfulAboutUs.secondText}</small>
              </p>
            </div>
            <div className="position-relative">
              <img
                src={data.contentfulAboutUs.secondIcon.file.url}
                alt="img"
              />
              <p className="yellow-text my-2">
                <small>{data.contentfulAboutUs.thirdText}</small>
              </p>
            </div>
          </div>
        </div>


      )}
    />
  )
}
