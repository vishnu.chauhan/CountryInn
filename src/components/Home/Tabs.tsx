import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const Tabs: React.FC<any> = props => {
  return (
    <StaticQuery
      query={graphql`
        query tabsQuery {
          allContentfulHomeTab(limit: 3) {
            edges {
              node {
                title
                subTitle
                description {
                  description
                }
                body {
                  body
                }
                backgroundImage {
                  file {
                    url
                    fileName
                  }
                  title
                }
                heading
                iconImage {
                  file {
                    fileName
                    url
                  }
                }
                slug
                subHeading
               
              }
            }
          }
        }
      `}
      render={data => (

        <div className="row">
          <div className="tab-content w-100">
            {data.allContentfulHomeTab &&
              data.allContentfulHomeTab.edges.map((item, key) => (
                <div
                  id={item.node.slug}
                  className={`${key == 0 ? "w-100 tab-pane active" : "w-100 tab-pane"
                    }`}
                  key={key}
                >
                  <div
                    className="tab-container"
                    style={{
                      background:
                        "url('" + item.node.backgroundImage.file.url + "')",
                    }}
                  >
                    <div className="container">
                      <div className="row">
                        <div className="col-lg-6">
                          <div className="tab-box position-relative pb-5 mt-5">
                            <div className="pt-4 pb-0 px-3">
                              <div className="row">
                                <div className="col-sm-8">
                                  <h3 className="wedding-box-text yellow-text tk-gautreaux">
                                    {item.node && item.node.heading}
                                    <span className="text-white">
                                      {item.node && item.node.subHeading}
                                    </span>
                                  </h3>
                                </div>
                                <div className="col-sm-4">
                                  <img
                                    src={
                                      item.node &&
                                      item.node.iconImage &&
                                      item.node.iconImage.file.url
                                    }
                                    className="d-block mx-auto couple-icon"
                                    alt="img"
                                  />
                                </div>
                              </div>
                              <p className="text-uppercase yellow-text-dark">
                                {item.node &&
                                  item.node.body &&
                                  item.node.body.body}
                              </p>
                              <p>
                                <small className="text-white font-weight-light d-none-340">
                                  {item.node &&
                                    item.node.description &&
                                    item.node.description.description}
                                </small>
                              </p>
                            </div>
                            <div className="text-right">
                              {/* <Link
                                to="/meetings-events"
                                className="tabs-view-button font-weight-light "
                              >
                                VIEW MORE
                              </Link> */}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
          </div>
        </div>


      )}
    />
  )
}
