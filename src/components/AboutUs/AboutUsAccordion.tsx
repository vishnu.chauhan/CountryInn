import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const AboutUsAccordion: React.FC<any> = props => {

    const { contentfulAboutUsPage } = props;

    return (

        <section className="container-xxl py-5 hotals-tabs term-condition">
            <div className="row pt-5">
                <div className="container">
                    <article className="tabbed-content">
                        <nav className="tabs">
                            <ul className="list-inline d-flex">
                                <li><a href="#tab1" className="active">EXPERIENCE</a></li>
                                <li><a href="#tab2">AFFILIATIONS</a></li>
                                <li><a href="#tab3">ECO FRIENDLY</a></li>
                            </ul>
                        </nav>
                        <div id="tab1" className="item active" data-title="EXPERIENCE">
                            <div className="container-fluid item-content">
                                <h3 className="yellow-text-dark tk-gautreaux mt-4 mt-lg-2">Experience</h3>
                                <p>{contentfulAboutUsPage.experienceTabContent.experienceTabContent}</p>
                                {/* <p>
                                    Each of our hotels and resorts is creatively designed to reflect the exclusive tastes
                                    and sensitivities of our much valued patrons. No wonder 'holidays' take a new dimension
                                    at Country Inn! At our hotels and resorts we offer an awesome combination of luxurious
                                    infrastructure with lush green nature as its unique backdrop. Constantly keeping up with
                                    international trends in the holiday industry, we aim to be the country's number one
                                    getaway in eco-tourism. </p>
                                <p> While each of our hotels and resorts gives you a unique experience in its flavour and
                                    ambience, what is consistent between each of our resorts is our passion for helping our
                                    guests to explore the destination, facilitate the objective, and our passion for
                                    delivering exceptional hotels and resorts experiences.</p>
                                <p> Our hotels and resorts are located at unbelievable natural junctions: flanked by great
                                    Himalayan Mountains, by alongside tranquil lakes, in the midst of virgin forests,
                                    bordering wildlife sanctuaries and also on the speedway to the most charming of
                                    monuments the Taj Mahal at Agra. Our hotels and resorts at Amritsar,Bhimtal, Corbett,
                                    Sattal, Mussoorie and Kosi-Kalan are places suited not only for adventure and discovery,
                                    but also are places that inspire the soul. Places for people to grow together. Places
                                    with a high level of service and an exacting standard of hospitality that can only be
                                    measured by the frequency of repeat visits by our patrons.
                                </p> */}
                            </div>
                        </div>
                        <div id="tab2" className="item" data-title="AFFILIATIONS">
                            <div className="container-fluid item-content">
                                <h3 className="yellow-text-dark tk-gautreaux mt-4 mt-lg-2">Affilications</h3>
                                <ul className="mt-3 text-dark ">
                                    {contentfulAboutUsPage.affilicationsList.map((item, index) => (
                                        <li key={index}> <i className="fa fa-check"></i>{item}
                                        </li>

                                    ))}
                                </ul>
                            </div>
                        </div>
                        <div id="tab3" className="item" data-title="ECO FRIENDLY">
                            <div className="container-fluid item-content">
                                <h3 className="yellow-text-dark tk-gautreaux mt-4 mt-lg-2">Eco Friendly</h3>
                                <p>{contentfulAboutUsPage.ecofriendlyParaContent.ecofriendlyParaContent}</p>
                                {/* <p>The concept of tourism existed since time immemorial when man travelled far and wide in
                                    pursuit of pleasure or spirituality. However the tourism boom began with the advent of
                                    faster modes of travel. Shipping was the first to come up followed by the locomotive,
                                    the motor car and finally the air plane. Each has contributed to travel immensely by
                                    cutting down the travel time drastically. Along with the industrial revolution came
                                    disposable incomes which have today made travel affordable to the vast majority.</p>
                                <p>However while technology may have made travel easy, it has not been able to keep pace
                                    with the rate of depletion of natural resources at tourist destinations that came along
                                    with the heavy influx of human population in the vicinity. The perceivable adverse
                                    effects have been so profound that it is today believed that Tourism is a major
                                    contributor to environmental degradation.</p>
                                <p>This realization lead to the development of the concept of eco-tourism in 2002 and at
                                    Country Inn we are fully aware of our role in the environments that we operate in. From
                                    the hills of Bhimtal to the forests of Corbett and further to the lakes at Sattal we are
                                    leaving no stone unturned to ensure the lowest possible borrowings from Mother Nature.
                                    In fact in all our processes of hotel operations we keep a tab on the environmental
                                    implications of what we do. </p> */}
                                <h6 className="text-dark">{contentfulAboutUsPage.ecofriendlyListHeading.ecofriendlyListHeading}</h6>
                                <ul className="mt-3 text-dark">
                                    {contentfulAboutUsPage.ecofriendlyList.map((item, index) =>
                                        <li key={index}> <i className="fa fa-check"></i>
                                            {item}
                                        </li>
                                    )}

                                </ul>
                                <h6 className="text-dark">{contentfulAboutUsPage.ecofriendlyListHeading2}</h6>
                                <ul className="mt-3 text-dark">
                                    {contentfulAboutUsPage.ecofriendlyProductlist.map((item, index) => (
                                        <li key={index}> <i className="fa fa-check"></i>
                                            {item}
                                        </li>
                                    ))}
                                </ul>

                            </div>
                        </div>

                    </article>
                </div>
            </div>
        </section>



    )
}