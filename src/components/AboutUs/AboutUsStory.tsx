import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const AboutUsStory: React.FC<any> = props => {

    const { contentfulAboutUsPage } = props;

    return (

        <section style={{ backgroundImage: "url('./images/carousel-img.png')" }} className="container-xxl meeting-content">
            <div className="container">
                <div className="row">
                    <div className="col-md-12 py-5 ">
                        <div className="my-lg-5">
                            <h1 className="tk-gautreaux text-center font-weight-bold yellow-text">Our Story </h1>
                            <div className=" mt-4">
                                <p className="text-white mt-4">{contentfulAboutUsPage.storyPara1.storyPara1}

                                </p>
                                <p className="text-white mt-4">{contentfulAboutUsPage.storyPara2.storyPara2}

                                </p>
                                <p className="text-white mt-4">{contentfulAboutUsPage.storyPara3.storyPara3}

                                </p>
                                <p className="text-white mt-4">{contentfulAboutUsPage.storyPara4.storyPara4}

                                </p>
                                <p className="text-white mt-4">{contentfulAboutUsPage.storyPara5.storyPara5}

                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    )
}