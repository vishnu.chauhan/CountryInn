import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const PartnerWithUsBrandingBenefits: React.FC<any> = props => {

    const { allContentfulPartnerWithUs } = props

    return (

        <div className="business-container bg-light-pink py-5 container-xxl bg-light-pink">

            <section className="business-section mt-lg-4">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="pr-lg-5">
                                <h1 className="yellow-text-dark1 tk-gautreaux"> {allContentfulPartnerWithUs.edges[0].node.subDescription.subDescription}</h1>
                                <p className="text-dark mt-3">{allContentfulPartnerWithUs.edges[0].node.description.description}
                                    {/* At<strong> Country Inn Hotels and Resorts </strong>, we believe that it is very
                                    important for a single hotel to join the network of a mature, well recognised brand
                                    to <strong> maximize business opportunity </strong>. Constant upgrading of the
                                    existing operating hotels and commissioning of new hotels is a conscious effort. Our
                                    properties continue to be market leaders in terms of operational excellence, guest
                                    satisfaction and profitability. */}
                                </p>
                                <p className="text-dark">{allContentfulPartnerWithUs.edges[0].node.descriptionpara2.descriptionpara2}
                                    {/* The Group regards its loyal and experienced staff to be its most important asset.
                                    Significant efforts are made into staff training, career development, staff
                                    well-being and communications, taking account of local culture and traditions as far
                                    as possible. As a result, the Group enjoys strong loyalty from its staff with many
                                    long-serving members. By doing things professionally and delivering memorable guest
                                    experiences, <strong> Country Inn Hotels and Resorts </strong> is realizing results.
                                    We are striving to improve our service every day. If you have the desire to become
                                    part of a highly renowned Indian hotel brand, give us a call or write to us. */}
                                </p>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="brand-img-container pt-1" style={{ backgroundImage: "url('./images/branding.jpg')" }}>
                                <div className="branding-container p-4 p-lg-5">
                                    <h1 className="yellow-text tk-gautreaux ">Branding
                                        <span className="text-white">Benefits</span>
                                    </h1>
                                    <ul className="text-white branding-ul mt-3 mt-lg-4">
                                        {allContentfulPartnerWithUs.edges[0].node.points.bulletPoints.map((item, index) => (
                                            <li key={index}>{item}</li>
                                        ))}
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </div>




        // <div className="container-xxl position-realtive contact-hr py-5 mb-0 ">
        //     <div className="container">
        //         <div className="row  mt-5 pt-lg-4">

        //             <div className="col-lg-6 justify-content-center mx-auto">
        //                 <h1 className="yellow-text tk-gautreaux">
        //                     {allContentfulPartnerWithUs.edges[0].node.subDescription.subDescription}
        //                     <br />
        //                 </h1>
        //                 <p className="yellow-text ">
        //                     {allContentfulPartnerWithUs.edges[0].node.description.description}
        //                 </p>
        //             </div>


        //             <div className="col-lg-6 justify-content-center"
        //                 style={
        //                     {
        //                         background: "url('images/carousel-img.png')",
        //                     }
        //                 }>
        //                 <div className="row justify-content-center my-4">
        //                     <div className="col-lg-10"
        //                         style={{
        //                             backgroundColor: 'rgba(0, 0, 0, 0.486)',
        //                             color: 'white',
        //                             borderRadius: '1rem'

        //                         }} >
        //                         <h3 className="tk-gautreaux py-5">
        //                             {allContentfulPartnerWithUs.edges[0].node.imageHeading}{" "}
        //                             <span className="  yellow-text  ">
        //                                 {allContentfulPartnerWithUs.edges[0].node.imageSubHeading} </span>
        //                         </h3>
        //                         <div className="container">

        //                             <div className="row ">
        //                                 {allContentfulPartnerWithUs.edges[0].node.points.bulletPoints.map((item, key) => (
        //                                     <div className="col-md-6 mb-5 mx-auto">
        //                                         <h4 >{item}</h4>
        //                                     </div>
        //                                 ))}

        //                             </div>
        //                         </div>
        //                     </div>
        //                 </div>
        //             </div>
        //         </div>
        //     </div >
        // </div >
    )

}