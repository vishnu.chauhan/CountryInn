import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const PartnerWithUsFixedText: React.FC<any> = props => {

    const { allContentfulPartnerWithUs } = props;
    const { textonlycomponentData } = allContentfulPartnerWithUs.edges[0].node

    //console.log(textonlycomponentData);

    const renderData = (item) => {
        if (item.subHeading == "null") {
            return (
                <div className="col-md-6  mt-3 mt-lg-4">
                    <h3 className="text-white  font-weight-600">{item.heading}</h3>
                    <p className="text-white content">{item.content.content}</p>
                </div>
            )
        }
        else if (item.content.content == "null") {
            return (
                <div className="col-md-6 d-none d-md-block">
                    <h1 className="yellow-text mt-lg-4 tk-gautreaux ">{item.heading}{" "}
                        <span className="text-white">{item.subHeading} </span>
                    </h1>
                </div>
            )

        }
        else {
            return (
                <div className="col-md-6">
                    <h3 className="yellow-text font-weight-600 ">{item.heading}{" "}
                        <span className="text-white">{item.subHeading} </span>
                    </h3>
                    <p className="text-white content">{item.content.content}</p>
                </div>
            )
        }


    }

    return (

        <section className="whoweare-container py-5 container-xxl" style={{ backgroundImage: "url('./images/bg-home-bottom.png')" }}>

            <div className="container mt-lg-4 whoweare-section">
                <div className="row">
                    <div className="col-md-6 d-md-none">
                        <h1 className="yellow-text tk-gautreaux ">We’ve Got You{" "}
                            <span className="text-white">Covered </span>
                        </h1>
                    </div>
                    {textonlycomponentData.map((item, index) => (
                        renderData(item)
                    ))}
                </div>
            </div>

        </section>

    )

}
