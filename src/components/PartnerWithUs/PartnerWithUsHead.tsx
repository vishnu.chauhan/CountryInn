import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"
import { ContactDetailleft } from "../Header/ContactDetailleft"
import { BookNow } from "../Header/BookNow"
import { BookNowMobile } from "../Header/BookNowMobile"
import { SocialMedia } from "../Header/SocialMedia"
import { BannerImageName } from "../Header/BannerImageName"

export const PartnerWithUsHead: React.FC<any> = props => {

    const { allContentfulPartnerWithUs } = props

    return (
        <>
            <div className="container-xxl  position-relative">
                <div className="row">
                    <div className="home-page-banner">
                        <div id="homeCarousel">
                            <div className="main">
                                <div className="slider slider-for-img">
                                    {allContentfulPartnerWithUs.edges.map((item, key) => (
                                        <div className="position-relative">
                                            <img
                                                src={`${item.node.imageHead.file.url}`}
                                                className="d-block w-100"
                                                alt="Img"
                                            />
                                            <div className="carousel-caption text-left pl-4">
                                                <h3 className="tk-gautreaux ">
                                                    {/* {item.node.heading}{""} */}
                                                    {/* <span className="  yellow-text "> {item.node.subHeading} </span> */}
                                                    <span className="  yellow-text ">Partnership</span>
                                                </h3>

                                            </div>
                                        </div>
                                    ))}
                                </div>

                            </div>
                            <BookNow />
                            <ContactDetailleft />
                            <div className="d-none d-md-flex right-black-strip">
                                {/* 
                                <div className="carousel-address ml-2">
                                 <BannerImageName />
                                </div>
                                 */}
                                <div >
                                    <span className="strip-gray"></span>
                                    <a href="" className="text-uppercase">
                                        {" "}<i className="fa fa-map-marker"></i> Partner with us</a>
                                </div>
                                <div>
                                    <ul className="list-in d-block  sidebar-social-media">
                                        <SocialMedia disaply="list-inline-items" />
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <BookNowMobile />

        </>
    )
}