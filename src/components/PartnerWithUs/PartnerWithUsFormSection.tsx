import React from "react"
import { useState } from 'react';
import { PartnerFormValidation } from "./PartnerFormValidation"

export const PartnerWithUsFormSection: React.FC<any> = props => {

    const { allContentfulPartnerWithUs } = props

    const [city, setCity] = useState('Select City');
    const [fname, setFirstName] = useState('')
    const [lname, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [msg, setMessage] = useState('')
    const [pnumber, setNumber] = useState('')
    const [fnameval, setFisrtNameVal] = useState('')
    const [numberval, setNumberVal] = useState('')
    const [lnameval, setLastNameVal] = useState('')
    const [emailval, setEmailVal] = useState('')
    const [cityVal, setCityVal] = useState('')
    const [errors, setErrors] = useState({ fname: '', })
    const [successSubmit, setsuccessSubmit] = useState('');

    const onSubmit = (e) => {
        e.preventDefault();
        setsuccessSubmit('')
        //setErrors([])
        const data = {
            city,
            fname,
            lname,
            email,
            msg,
            pnumber
        }

        PartnerFormValidation(data)
            .then(response => {
                console.log("Data submitted successfully...", response)
                postData(response)
                setsuccessSubmit('Your response is submitted..')
                setCity('Select City')
                setFirstName('')
                setLastName('')
                setEmail('')
                setMessage('')
                setNumber('')
                setFisrtNameVal('')
                setNumberVal('')
                setLastNameVal('')
                setEmailVal('')
                setCityVal('')

                //setErrors([])
            })
            .catch(err => {
                //setErrors([...errors, err])
                //setErrors({ ...errors, fname: err.fname })
                setFisrtNameVal(err.fname)
                setNumberVal(err.pnumber)
                setLastNameVal(err.lname)
                setEmailVal(err.email)
                setCityVal(err.city)
                console.log("Validation Error...", err)
            })

    }

    const postData = async (value) => {

        const res = await fetch('https://emailsender-shivendra.herokuapp.com/api/partners', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(value)
        })
        const data = await res.json()
    }

    return (
        <div className="partner-container meeting-content py-5 container-xxl"
            style={{ backgroundImage: "url('./images/hero-banner.jpg')" }}>
            <section className="partner-section mt-lg-4">
                <div className="col-lg-8 offset-lg-2">
                    <h1 className="yellow-text tk-gautreaux text-center mx-auto ">{allContentfulPartnerWithUs.edges[0].node.formHeading}</h1>
                    <h4 className="text-white font-weight-normal ls-2 text-center mt-3 mt-lg-4">
                        {allContentfulPartnerWithUs.edges[0].node.formSubHeading}
                    </h4>
                    <div className="row">
                        <div className="col-lg-8 offset-lg-2">
                            <h4 className="my-3 ml-2 mt-lg-5 font-weight-600 text-white"> Partner With Us</h4>
                            <form className="form ">
                                <div className="row w-100 no-gutters">
                                    <div className="col-md-6">
                                        <select
                                            value={city}
                                            onChange={(e) => setCity(e.target.value)} >
                                            <option selected disabled>Select City</option>
                                            <option>Amritsar</option>
                                            <option>Badaun</option>
                                            <option>Haridwar</option>
                                            <option>Corbet</option>
                                            <option>Bhimtal</option>
                                            <option>Vrindavan</option>
                                        </select>
                                        {/* <small>{errors && errors[0] && errors[0].city}</small> */}
                                        <small>{cityVal}</small>
                                    </div>
                                    <div className="col-md-6">
                                        <input
                                            type="phone"
                                            placeholder="Phone Number "
                                            name=""
                                            id=""
                                            value={pnumber}
                                            onChange={(e) => setNumber(e.target.value)}>
                                        </input>
                                        {/* <small>{errors && errors[0] && errors[0].pnumber}</small> */}
                                        <small>{numberval}</small>

                                    </div>
                                    <div className="col-md-6">
                                        <input
                                            type="text"
                                            placeholder="First Name"
                                            name=""
                                            id=""
                                            value={fname}
                                            onChange={(e) => setFirstName(e.target.value)}>
                                        </input>

                                        {/* <small>{errors && errors[0] && errors[0].fname}</small> */}
                                        <small>{fnameval}</small>

                                    </div>
                                    <div className="col-md-6">
                                        <input
                                            type="text"
                                            placeholder="Last Name "
                                            name=""
                                            id=""
                                            value={lname}
                                            onChange={(e) => setLastName(e.target.value)}
                                        />
                                        {/* <small>{errors && errors[0] && errors[0].lname}</small> */}
                                        <small>{lnameval}</small>
                                    </div>
                                    <div className="col-md-12">
                                        <input
                                            type="email"
                                            placeholder="Email Address"
                                            name=""
                                            id=""
                                            value={email}
                                            onChange={(e) => setEmail(e.target.value)}
                                        />
                                        {/* <small>{errors && errors[0] && errors[0].email}</small> */}
                                        <small>{emailval}</small>
                                    </div>
                                    <div className="col-md-12">
                                        <textarea
                                            className="h-auto"
                                            placeholder="Message (Optional)"
                                            rows={4}
                                            value={msg}
                                            onChange={(e) => setMessage(e.target.value)}
                                        >

                                        </textarea>

                                    </div>
                                    <div className="col-md-12 pr-3">
                                        <button className="book-button text-14 w-100 font-weight-bold position-relative mt-3 border-0 py-3"
                                            type="button"
                                            onClick={onSubmit}
                                        >
                                            SUBMIT</button>
                                    </div>
                                    <small>{successSubmit}</small>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>

    )
}




