export const PartnerFormValidation = data => {

    return new Promise((resolve, reject) => {

        console.log("Validation...", data.fname.length)
        let errors = {}
        let formIsValid = true
        //Name
        if (data.fname.length === 0) {
            formIsValid = false
            errors["fname"] = "Please enter First Name"
            // errors = {
            //     fname: "Please enter First Name"
            // }
            reject(errors)
        } else if (!data.fname.match(/^[a-zA-Z]+$/)) {
            formIsValid = false
            errors["fname"] = "Please enter only letters"
            // errors = {
            //     fname: "Please enter only letters"
            // }
            reject(errors)
        }
        if (data.city == "Select City") {
            formIsValid = false
            errors["city"] = "Please select a city"
            // errors = {
            //     city: "Please Select a City"
            // }

            reject(errors)
        }
        if (data.pnumber.length === 0) {
            formIsValid = false
            errors["pnumber"] = "Please enter a Phone Number"
            // errors = {
            //     pnumber: "Please enter a Phone Number"
            // }
            reject(errors)
        } else if (!data.pnumber.match(/^[0-9]+$/)) {
            formIsValid = false
            errors["pnumber"] = "Please enter only numbers"
            // errors = {
            //     pnumber: "Please enter only Numbers"
            // }
            reject(errors)
        }
        if (data.lname.length === 0) {
            formIsValid = false
            errors["lname"] = "Please enter Last Name"
            // errors = {
            //     lname: "Please enter Last Name"
            // }

            reject(errors)
        } else if (!data.lname.match(/^[a-zA-Z]+$/)) {
            formIsValid = false
            errors["lname"] = "Please enter only letters"
            // errors = {
            //     lname: "Please enter only letters"
            // }
            reject(errors)
        }
        if (data.email.length === 0) {
            formIsValid = false
            errors["email"] = "Please enter Email Address"
            // errors = {
            //     email: "Please enter an Email"
            // }
            reject(errors)
        } else if (data.email.length > 0) {
            let lastAtPos = data.email.lastIndexOf("@")
            let lastDotPos = data.email.lastIndexOf(".")
            if (
                !(
                    lastAtPos < lastDotPos &&
                    lastAtPos > 0 &&
                    data.email.indexOf("@@") === -1 &&
                    lastDotPos > 2 &&
                    data.email.length - lastDotPos > 2
                )
            ) {
                formIsValid = false
                errors["email"] = "Please enter valid email ID"
                // errors = {
                //     email: "Please enter valid email ID"
                // }
                reject(errors)
            }
        }
        //this.setState({ errors: errors })
        resolve(data)

    })
}