import React from "react"
import { Link, graphql } from "gatsby"


export const DestinationRestaurantandFacilities: React.FC<any> = props => {
    var { contentfulDestination } = props


    return (
        <div
            className="home-container py-5 container-xxl"
            style={{
                background:
                    "url('https://res.cloudinary.com/espire/image/upload/v1620205113/samples/images/bg-home-bottom.png')",
            }}
        >
            <div className="row">
                <div className="container hotals-tabs bg-transparent">
                    <article className="tabbed-content hotal-two-tabs w-100">
                        <nav className="tabs col-md-10 col-lg-6 offset-lg-3 offset-md-1">
                            <ul className="list-inline d-flex">
                                {contentfulDestination &&
                                    contentfulDestination.restaurantAndFacilitiesTab &&
                                    contentfulDestination.restaurantAndFacilitiesTab.map(
                                        (tab, key) => (
                                            <li key={key}>
                                                <a
                                                    href={`#${tab.slug}`}
                                                    className={`${key == 0 ? "active" : ""}`}
                                                >
                                                    {tab.title}
                                                </a>
                                            </li>
                                        )
                                    )}
                            </ul>
                        </nav>

                        {contentfulDestination &&
                            contentfulDestination.restaurantAndFacilitiesTab &&
                            contentfulDestination.restaurantAndFacilitiesTab.map(
                                (tab, key) => (
                                    <section
                                        id={`${tab.slug}`}
                                        className={`${key == 0 ? "item active" : "item "}`}
                                        data-title="Tab 1"
                                        key={key}
                                    >
                                        <div className="row">
                                            {contentfulDestination &&
                                                contentfulDestination.restaurantAndFacilities &&
                                                contentfulDestination.restaurantAndFacilities.length >
                                                0 &&
                                                contentfulDestination.restaurantAndFacilities.map(
                                                    (restroItem, key) =>
                                                        restroItem.tag == tab.slug && (
                                                            <div
                                                                className="col-lg-6 item-content"
                                                                key={key}
                                                                style={{ marginBottom: "50px" }}
                                                            >
                                                                <div className="hotal-four-grid ">
                                                                    <div className="hfg-img mt-4">
                                                                        <img
                                                                            src={`${restroItem &&
                                                                                restroItem.image &&
                                                                                restroItem.image.file &&
                                                                                restroItem.image.file.url
                                                                                }`}
                                                                            alt="img"
                                                                        />
                                                                    </div>
                                                                    <div className="hfg-content mt-3">
                                                                        <h4 className="yellow-text text-uppercase">
                                                                            {restroItem.title}
                                                                            <span className="d-block text-white ">
                                                                                {restroItem.subTitle}
                                                                            </span>
                                                                        </h4>
                                                                        <p className="text-14 text-white">
                                                                            {restroItem.description.description}
                                                                        </p>

                                                                        {restroItem &&
                                                                            restroItem.tableData &&
                                                                            restroItem.tableData.tableData && (
                                                                                <table className="table-hotal w-100 mt-3">
                                                                                    {restroItem.tableData.tableData.map(
                                                                                        (tableitem, key) =>
                                                                                            key == 0 ? (
                                                                                                <thead className="bg-yellow">
                                                                                                    <tr>
                                                                                                        {tableitem.map(
                                                                                                            (item, key) => (
                                                                                                                <td> {item} </td>
                                                                                                            )
                                                                                                        )}
                                                                                                    </tr>
                                                                                                </thead>
                                                                                            ) : (
                                                                                                <tr>
                                                                                                    {tableitem.map(
                                                                                                        (item, key) => (
                                                                                                            <td> {item} </td>
                                                                                                        )
                                                                                                    )}
                                                                                                </tr>
                                                                                            )
                                                                                    )}
                                                                                </table>
                                                                            )}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        )
                                                )}
                                        </div>
                                    </section>
                                )
                            )}
                    </article>
                </div>
            </div>
        </div>
    )
}