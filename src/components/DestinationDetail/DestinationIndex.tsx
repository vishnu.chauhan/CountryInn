import React from "react"
import { Link, graphql } from "gatsby"
import { SocialMedia } from "../Header/SocialMedia"
import { BookNow } from "../Header/BookNow"
import { ContactDetailleft } from "../Header/ContactDetailleft"
import { Banner } from "../Header/Banner"
import { DestinationRoomTypes } from "./DestinationRoomTypes"
import { DestinationImageSlider } from "./DestinationImageSlider"
//import { DestinationRestaurantandFacilities } from "./DestinationRestaurantandFacilities"
import { DestinationFacilitiesNew } from "./DestinationFacilitiesNew"
import { BookNowMobile } from "../Header/BookNowMobile"

export const DestinationIndex: React.FC<any> = props => {
  var { contentfulDestination } = props
  var imgArr = contentfulDestination.banner.bannerItems
  console.log(contentfulDestination)

  return (
    <>
      <div className="container-xxl  position-relative">
        <div className="row">
          <div className="home-page-banner">
            <div id="homeCarousel">
              <div className="main">
                <div className="slider desktop-slider slider-for-img">
                  {imgArr.map((item, key) => (
                    <div className="position-relative" key={key}>
                      <img
                        src={`${item.image.file.url}`}
                        className="d-block w-100"
                        alt="Img"
                      />

                      {/* <div className="carousel-caption">
                        <h3 className="tk-gautreaux">
                          {item.title}{" "}
                          <span className="  yellow-text ">
                            {" "}
                            {item.subTitle}{" "}
                          </span>
                        </h3>
                        <p className="text-uppercase">
                          {item.description && item.description.description}
                        </p>
                      </div> */}
                    </div>
                  ))}
                </div>
                <div className="slider mobile-slider slider-for-img">
                  {imgArr.map((item, key) => (
                    <div className="position-relative">
                      <img
                        src={`${item.mobileViewImg.file.url}`}
                        className="d-block w-100"
                        alt="..."
                      />
                    </div>
                  ))}
                </div>
              </div>
              <BookNow />
              <ContactDetailleft />
              <div className="d-none d-md-flex right-black-strip">
                <div className="carousel-address ml-2">
                  <div className="slider slider-nav-address">
                    {imgArr.map((item, key) => (
                      <div className="position-relative" key={key}>
                        <span className="strip-gray"></span>
                        <a href="" className="text-uppercase">
                          &nbsp;
                          <i className="fa fa-map-marker"></i>{" "}
                          {item.imageDetail}
                        </a>
                      </div>
                    ))}
                  </div>
                </div>
                <div>
                  <ul className="list-in d-block  sidebar-social-media">
                    <SocialMedia disaply="list-inline-items" />
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div
        className="container-xxl position-relative hotel-location contact-our-address"
        data-color="red"
        style={{
          background:
            "url('https://res.cloudinary.com/espire/image/upload/v1620205113/samples/images/bg-home-bottom.png')",
        }}
      >
        <div className="container pt-5 pb-4">
          <div className="row">
            <DestinationImageSlider
              contentfulDestination={contentfulDestination}
            />
            <div className="col-md-6">
              <h1 className=" mt-lg-5 mt-lg-4 yellow-text tk-gautreaux">
                {contentfulDestination.subTitle}
                <span className="text-white">
                  &nbsp;{contentfulDestination.title}
                </span>
              </h1>
              <div className="rating">
                4.6 <i className="fa fa-star"></i>
              </div>
              <div className="text-white mt-4">
                <p>{contentfulDestination.description.description}</p>
                <div className="row  mt-3 mt-lg-4 yellow-text">
                  {contentfulDestination &&
                    contentfulDestination.facilities &&
                    contentfulDestination.facilities.length > 0 &&
                    contentfulDestination.facilities.map((itemData, key) => (
                      <div className="col-lg-6" key={key}>
                        <p>
                          <img
                            className="mr-3"
                            width="40"
                            src={itemData.icon && itemData.icon.file.url}
                            alt="img"
                          />
                          {itemData.title}
                        </p>
                      </div>
                    ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <BookNowMobile />

      {/* component for Room Types */}

      <DestinationRoomTypes contentfulDestination={contentfulDestination} />

      {/* component for RestaurantandFacilities  */}

      {/* <DestinationRestaurantandFacilities contentfulDestination={contentfulDestination} /> */}
      <DestinationFacilitiesNew contentfulDestination={contentfulDestination} />
    </>
  )
}
