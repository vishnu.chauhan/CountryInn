import React from "react"


export const DestinationImageSlider: React.FC<any> = props => {
    var { contentfulDestination } = props
    console.log(contentfulDestination)

    return (

        <div className="col-md-6 pr-lg-5">
            <div className="main my-lg-3">
                <div className="slider hotal-thumb-big slider-for">
                    {contentfulDestination &&
                        contentfulDestination.gallery &&
                        contentfulDestination.gallery.images.length > 0 &&
                        contentfulDestination.gallery.images.map(
                            (imageData, key) => (
                                <div key={key}>
                                    <img
                                        src={
                                            imageData && imageData.file && imageData.file.url
                                        }
                                        alt="img"
                                    />
                                </div>
                            )
                        )}
                </div>
                <div className="slider hotal-thumb slider-nav">
                    {contentfulDestination &&
                        contentfulDestination.gallery &&
                        contentfulDestination.gallery.images.length > 0 &&
                        contentfulDestination.gallery.images.map(
                            (caroItem, key) => (
                                <div key={key}>
                                    <img
                                        src={caroItem && caroItem.file && caroItem.file.url}
                                        alt="img"
                                    />
                                </div>
                            )
                        )}

                </div>
            </div>

            <a href={contentfulDestination.gallery.galleryPageLink} className="book-button">
                <span className="d-inline-block py-2 py-lg-3 pl-3 pr-2">View More</span>
                <span className="d-inline-block p-3 py-lg-3 bg-yellow-light"><i
                    className="fa fa-angle-double-right" aria-hidden="true"></i></span>
            </a>

        </div>

    )
}