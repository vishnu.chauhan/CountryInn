import React from "react"


export const DestinationRoomTypes: React.FC<any> = props => {
    var { contentfulDestination } = props
    var roomTypes = contentfulDestination.roomTypes

    return (

        <div className="container-xxl hotals-tabs">
            <div className="row  py-5">
                <div className="container">
                    <article className="tabbed-content ">
                        <nav className="tabs">
                            <ul className="list-inline d-flex">
                                {contentfulDestination &&
                                    contentfulDestination.roomTypes &&
                                    contentfulDestination.roomTypes.length > 0 &&
                                    contentfulDestination.roomTypes.map((roomItem, key) => (
                                        <li key={key}>
                                            <li>
                                                <a
                                                    href={`#${roomItem.slug}`}
                                                    className={`${key == 0 ? "active" : ""}`}
                                                >
                                                    {roomItem.heading}
                                                </a>
                                            </li>
                                        </li>
                                    ))}
                            </ul>
                        </nav>

                        {contentfulDestination &&
                            contentfulDestination.roomTypes &&
                            contentfulDestination.roomTypes.length > 0 &&
                            contentfulDestination.roomTypes.map((roomItem, key) => (
                                <section
                                    key={key}
                                    id={`${roomItem.slug}`}
                                    className={`${key == 0 ? "item active" : "item"}`}
                                    data-title="TWO BEDROOM deluxe cottages"
                                >
                                    <div className="container-fluid">
                                        <div className="row">
                                            <div className="col-lg-9 p-0">
                                                <div className="item-content">
                                                    <div className="htl-img ">
                                                        <img
                                                            src={`${roomItem.image.file.url}`}
                                                            alt="img"
                                                        />
                                                    </div>
                                                    <div className="htl-content  transparent-content">
                                                        <div className="py-5 px-3 px-lg-5">
                                                            <h2 className="tk-gautreaux yellow-text ">
                                                                {roomItem.title}
                                                                <span className="d-md-block text-white">
                                                                    {roomItem.subTitle}
                                                                </span>
                                                            </h2>
                                                            <div className="mt-md-4 mt-2">
                                                                <p>{roomItem.description.description}</p>
                                                            </div>
                                                            <a href={roomItem.bookNowLink} className="book-button">
                                                                <span className="d-inline-block py-2 py-lg-3 pl-3 pr-2">
                                                                    BOOK NOW
                                                                </span>
                                                                <span className="d-inline-block px-3 py-lg-3 bg-yellow-light">
                                                                    <i
                                                                        className="fa fa-angle-double-right"
                                                                        aria-hidden="true"
                                                                    ></i>
                                                                </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            ))}
                    </article>
                </div>
            </div>
        </div>
    )
}