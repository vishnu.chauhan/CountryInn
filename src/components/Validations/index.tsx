export const contactUsFormValidation = data => {
  return new Promise((resolve, reject) => {
    console.log("Validation...", data.firstName.length)
    let errors = {}
    let formIsValid = true
    //Name
    if (data.firstName.length === 0) {
      formIsValid = false
      errors["firstName"] = "Please enter First Name"
      reject(errors)
    } else if (!data.firstName.match(/^[a-zA-Z]+$/)) {
      formIsValid = false
      errors["firstName"] = "Please enter only letters"
      reject(errors)
    }
    if (data.lastName.length === 0) {
      formIsValid = false
      errors["lastName"] = "Please enter Last Name"
      reject(errors)
    } else if (!data.lastName.match(/^[a-zA-Z]+$/)) {
      formIsValid = false
      errors["lastName"] = "Please enter only letters"
      reject(errors)
    }
    if (data.email.length === 0) {
      formIsValid = false
      errors["email"] = "Please enter Email Address"
      reject(errors)
    } else if (data.email.length > 0) {
      let lastAtPos = data.email.lastIndexOf("@")
      let lastDotPos = data.email.lastIndexOf(".")
      if (
        !(
          lastAtPos < lastDotPos &&
          lastAtPos > 0 &&
          data.email.indexOf("@@") === -1 &&
          lastDotPos > 2 &&
          data.email.length - lastDotPos > 2
        )
      ) {
        formIsValid = false
        errors["email"] = "Please enter valid email ID"
        reject(errors)
      }
    }
    //this.setState({ errors: errors })
    resolve(data)
    //return formIsValid
  })
}
