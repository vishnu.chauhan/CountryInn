import React from "react"
import { Link, graphql } from "gatsby"

export const DestinationGellary: React.FC<any> = props => {
  var { contentfulGallery } = props
  return (
    <>
      <div className="gallery py-5 bg-light-pink">
        <div className="container">
          <h1 className=" mt-lg-4 mt-3 mb-4 mb-lg-5 yellow-text-dark text-center tk-gautreaux">
            Country Inn &nbsp;
            <span>{contentfulGallery.title}</span>
          </h1>
          <div className="row">
            {contentfulGallery.images.map((item, key) => (
              <div className="col-md-4 col-6 col-lg-3 mb-2 px-1">
                <div className="img-gallery" key={key}>
                  <Link
                    to={item.file && item.file && item.file.url}
                    className="glry"
                    data-fancybox="images"
                    data-caption={item.description}
                  >
                    <img
                      className="about-grid-img"
                      src={item && item.file && item.file.url}
                      alt="img"
                    />
                    <div className="layer">{item.description}</div>
                  </Link>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </>
  )
}
