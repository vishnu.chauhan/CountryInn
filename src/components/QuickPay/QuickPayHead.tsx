import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"
import { ContactDetailleft } from "../Header/ContactDetailleft"
import { BookNow } from "../Header/BookNow"
import { BookNowMobile } from "../Header/BookNowMobile"
import { SocialMedia } from "../Header/SocialMedia"
import { BannerImageName } from "../Header/BannerImageName"

export const QuickPayHead: React.FC<any> = props => {

    const { contentfulQuickPay } = props

    return (
        <>
            <div className="container-xxl  position-relative">
                <div className="row">
                    <div className="home-page-banner">
                        <div id="homeCarousel">
                            <div className="main">
                                <div className="slider slider-for-img">
                                    {/* {allContentfulPartnerWithUs.edges.map((item, key) => ( */}
                                    <div className="position-relative">
                                        <img
                                            src={`${contentfulQuickPay.imageQuickPay.file.url}`}
                                            className="d-block w-100"
                                            alt="Img"
                                        />
                                        <div className="carousel-caption text-left pl-4">
                                            <h3 className="tk-gautreaux ">
                                                Quick Pay
                                                <span className="  yellow-text "> {contentfulQuickPay.name} </span>
                                            </h3>
                                            <p className="text-uppercase">
                                                Ubique iudicabit aliquando ex vix. Sea vidit mucius ei, his cu nihil legendos.
                                                Per nonumes posidonium ex.
                                            </p>
                                        </div>
                                    </div>
                                    {/* ))} */}
                                </div>

                            </div>
                            <BookNow />
                            <ContactDetailleft />
                            <div className="d-none d-md-flex right-black-strip">
                                {/* 
                                <div className="carousel-address ml-2">
                                 <BannerImageName />
                                </div>
                                 */}
                                <div >
                                    <span className="strip-gray"></span>
                                    <a href="" className="text-uppercase">
                                        {" "}<i className="fa fa-map-marker"></i>{contentfulQuickPay.imageQuickPay.description} </a>
                                </div>
                                <div>
                                    <ul className="list-in d-block  sidebar-social-media">
                                        <SocialMedia disaply="list-inline-items" />
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <BookNowMobile />

        </>
    )
}