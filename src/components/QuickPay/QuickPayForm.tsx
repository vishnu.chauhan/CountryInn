import React from "react"
import { useState } from 'react';
// import { PartnerFormValidation } from "./PartnerFormValidation"

export const QuickPayForm: React.FC<any> = props => {

    const { contentfulQuickPay } = props;

    return (
        <section>
            <div className=" py-5 bg-light-pink">
                <div className="container py-5">
                    <div className="row">
                        <div className="col-md-8 col-lg-6 offset-md-2 offset-lg-3">
                            <h6 className="yellow-text-dark1">Please provide Check-In & Check-Out date</h6>

                            <div className="invoice-form">
                                <h5 className="p-3 font-weight-bold yellow-text-light mb-0 border-bottom">Invoice Form</h5>
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-sm-6">
                                            <input type="text" placeholder="First Name" name="" id="" />
                                        </div>
                                        <div className="col-sm-6">
                                            <input type="text" placeholder="Last Name" name="" id="" />
                                        </div>
                                        <div className="col-sm-6">
                                            <input type="tel" placeholder="Mobile Number" name="" id="" />
                                            <small className="font-weight-light yellow-text-dark1">(Mobile Preferred)</small>
                                        </div>
                                    </div>
                                    <h5 className="pt-3 font-weight-bold yellow-text-light mb-0">Payment Details</h5>
                                    <div className="row">
                                        <div className="col-sm-6">
                                            <input type="text" placeholder="Payment Amount (INR)" name="" id="" />
                                        </div>
                                        <div className="col-sm-6">
                                            <input type="text" placeholder="Voucher Number" name="" id="" />
                                        </div>
                                        <div className="col-sm-12">
                                            <textarea rows={4} placeholder="Payment Purpose/Description"></textarea>
                                        </div>

                                        <div className="col-sm-12">
                                            <input type="button" className="bg-yellow font-weight-600 mt-3 mb-2 text-white" value="PAY ONLINE" />
                                            <div className="font-weight-light mb-3 yellow-text-dark1"><small >
                                                Note: Click on <strong>Pay Online</strong> button. You will be redirected to secured payment gateway to complete this transaction.
                                            </small>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div className="pt-4 text-center">
                                <img src={contentfulQuickPay.formLogoBottom.file.url} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

    )
}