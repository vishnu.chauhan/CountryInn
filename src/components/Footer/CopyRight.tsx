import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const CopyRight: React.FC<any> = props => {
  return (
    <StaticQuery
      query={graphql`
        query CopyRight {
          contentfulCopyRight {
            copyRight
            pages {
              slug
              title
            }
          }
        }
      `}
      render={data => (
        <>
          <li className="list-inline-item font-weight-200 text-12 text-white-50">
            {data.contentfulCopyRight && data.contentfulCopyRight.copyRight}
          </li>{" "}{"|"}{" "}
          <li className="list-inline-item">
            <Link
              to={`${data.contentfulCopyRight.pages[0].slug}`}
              className="text-12 font-weight-light text-white"
            >
              {data.contentfulCopyRight.pages[0].title}
            </Link>
          </li>{" "}{"|"}{" "}
          <li className="list-inline-item">
            <Link
              to={`${data.contentfulCopyRight.pages[1].slug}`}
              className="text-12 font-weight-light text-white"
            >
              {data.contentfulCopyRight.pages[1].title}
            </Link>
          </li>
          {/* {data.contentfulCopyRight.pages.map((item, key) => (
            <li className="list-inline-item" key={key}>
              <Link
                to={`${item.slug}`}
                className="text-12 font-weight-light text-white"
              >
                {item.title}
              </Link>
            </li>
          ))} */}
        </>
      )}
    />
  )
}
