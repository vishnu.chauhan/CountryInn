import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const QuickLinks: React.FC<any> = props => {
  return (
    <StaticQuery
      query={graphql`
        query QuickLinks {
          contentfulQuickLinks {
            name
            pages {
              title
              slug
            }
          }
        }
      `}
      render={data => (
        <>
          <h6>{data.contentfulQuickLinks && data.contentfulQuickLinks.name}</h6>
          <ul className="mt-md-4 mt-3 footer-link">
            {data.contentfulQuickLinks.pages.map((item, key) => (
              <li key={key}>
                <Link to={item.slug}>{item.title}</Link>
              </li>
            ))}
          </ul>
        </>
      )}
    />
  )
}
