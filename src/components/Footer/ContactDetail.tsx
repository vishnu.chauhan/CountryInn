import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const ContactDetail: React.FC<any> = props => {
  return (
    <StaticQuery
      query={graphql`
        query contactDetail {
          contentfulContactDetails {
            footerTime
            footerEmail
            countryCode
            headerMobile
            footerDescription {
              footerDescription
            }
          }
        }
      `}
      render={data => (
        <>
          <p className="text-white-50 my-4">
            <small>
              {data.contentfulContactDetails &&
                data.contentfulContactDetails.footerDescription
                  .footerDescription}
            </small>
          </p>
          <p>
            <Link
              to={`tel:(${
                data.contentfulContactDetails &&
                data.contentfulContactDetails.countryCode
              }) ${
                data.contentfulContactDetails &&
                data.contentfulContactDetails.headerMobile
              }`}
              className="text-white"
            >
              <small>
                (+
                {data.contentfulContactDetails &&
                  data.contentfulContactDetails.countryCode}
                ) &nbsp;
                {data.contentfulContactDetails &&
                  data.contentfulContactDetails.headerMobile}
                &nbsp;(
                {data.contentfulContactDetails &&
                  data.contentfulContactDetails.footerTime}
                )
              </small>
            </Link>
            <Link
              to={`mailto:${
                data.contentfulContactDetails &&
                data.contentfulContactDetails.footerEmail
              }`}
              className="text-white ml-4"
            >
              <small>
                {data.contentfulContactDetails &&
                  data.contentfulContactDetails.footerEmail}
              </small>
            </Link>
          </p>
        </>
      )}
    />
  )
}
