import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const TermsAndCondition: React.FC<any> = props => {
  return (
    <StaticQuery
      query={graphql`
        query TermsAndCondition {
          contentfulTermsAndConditions {
            title
            subTitle
            description {
              description
            }
          }
        }
      `}
      render={data => (
        <>
          <div className="container-xxl py-5 hotals-tabs term-condition">
            <div className="row pt-5">
              <div className="container">
                <article className="tabbed-content">
                  <nav className="tabs" id="termsConditionNav">
                    <ul className="list-inline d-flex">
                      <li>
                        <a href="#tab1" className="active">
                          Terms & Conditions
                        </a>
                      </li>
                      <li>
                        <a href="#tab2">Privacy Policy</a>
                      </li>
                      <li>
                        <a href="#tab3">Refund & Cancellation Policy</a>
                      </li>
                      <li>
                        <a href="#tab4">Child Policy</a>
                      </li>
                    </ul>
                  </nav>

                  <div
                    id="tab1"
                    className="item active"
                    data-title="Terms & Conditions"
                  >
                    <div className="container-fluid item-content">
                      <h1 className="yellow-text-dark tk-gautreaux">
                        Terms & Conditions
                      </h1>
                      <p>
                        {
                          data.contentfulTermsAndConditions.description
                            .description
                        }
                      </p>
                    </div>
                  </div>
                  <div id="tab2" className="item" data-title="Privacy Policy">
                    <div className="container-fluid item-content">
                      <h1 className="yellow-text-dark tk-gautreaux">
                        Privacy Policy
                      </h1>
                      <p>
                        Country Inn hotels & resorts collects information about
                        our guests and visitors to our web sites so that we can
                        provide an experience that is responsive to our guests
                        needs. We endeavor to collect information only with your
                        knowledge and with your permission if necessary. The
                        types of personally identifiable information that we
                        collect may include your name, home, work and e-mail
                        addresses, telephone and fax numbers, credit card
                        information, date of birth, gender, and lifestyle
                        information such as room preferences, leisure
                        activities, names and ages of children and other
                        information necessary to fulfill special requests (e.g.
                        Health conditions that require special room
                        accommodations).{" "}
                      </p>
                      <p>
                        {" "}
                        Country Inn hotels & resorts is fully committed to
                        providing you with information about the collection and
                        use of personally identifiable information furnished by
                        or collected from visitors while using our web sites,
                        products and services. It is our practice not to ask you
                        for information unless we need it or intend to use it.
                        To ensure that your personally identifiable information
                        is accurate and up to date, we encourage you to
                        regularly review and update your information as
                        appropriate (e.g. In the event your home or e- mail
                        address changes or you wish to add an additional method
                        for us to communicate with you). Country Inn hotels &
                        resorts recognizes the importance of information
                        security and is constantly reviewing and enhancing our
                        technical, physical, and logical security rules and
                        procedures to help protect your personally identifiable
                        information against loss, misuse and alteration while
                        under our control. Although 'guaranteed security' does
                        not exist either on or off the internet, we safeguard
                        your information using both procedural and technical
                        safeguards, including password controls, 'firewalls"
                        etc.
                      </p>
                    </div>
                  </div>
                  <div
                    id="tab3"
                    className="item"
                    data-title="Refund & Cancellation Policy"
                  >
                    <div className="container-fluid item-content">
                      <h1 className="yellow-text-dark tk-gautreaux">
                        Refund & Cancellation Policy
                      </h1>
                      <ul>
                        <li>
                          {" "}
                          <i className="fa fa-check"></i> 16 days or more before
                          check-in date: free cancellation
                        </li>
                        <li>
                          {" "}
                          <i className="fa fa-check"></i> between 15 days to 7
                          days: 50% of the total cost will be charged as
                          cancellation fee
                        </li>
                        <li>
                          {" "}
                          <i className="fa fa-check"></i> 1 to 7 days before
                          check-in date: no refund
                        </li>
                        <li>
                          {" "}
                          <i className="fa fa-check"></i> no-show & same day
                          cancellation: no refund
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div id="tab4" className="item" data-title="Child Policy">
                    <div className="container-fluid item-content">
                      <h1 className="yellow-text-dark tk-gautreaux">
                        Refund & Cancellation Policy
                      </h1>
                      <p>
                        Child below 6 years will be complimentary without an
                        extra bed and child between 6 to 12 years of age will be
                        chargeable.
                      </p>
                    </div>
                  </div>
                </article>
              </div>
            </div>
          </div>
        </>
      )}
    />
  )
}
