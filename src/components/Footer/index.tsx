import React from "react"
import { Link, graphql } from "gatsby"
import { SocialMedia } from "../Header/SocialMedia"
import { Logo } from "../Header/Logo"
import { Helmet } from "react-helmet"
import { QuickLinks } from "./QuickLinks"
import { ContactDetail } from "./ContactDetail"
import { CopyRight } from "./CopyRight"
import { useState, useEffect } from 'react';
import { contactUsFormValidation } from "../Validations"


var Recaptchafooter = require("react-recaptcha")
let recaptchaInstancefooter

export const Footer: React.FC<any> = ({ title }) => {

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('')
  const [email, setEmail] = useState('');
  const [isVerfied, checkVerified] = useState(false);
  const [captchaText, setCaptchaText] = useState('');
  const [fnameval, setFisrtNameVal] = useState('')
  const [lnameval, setLastNameVal] = useState('')
  const [emailval, setEmailVal] = useState('')
  const [successSubmit, setsuccessSubmit] = useState('');


  const onSubmit = (e) => {
    e.preventDefault();
    setsuccessSubmit(' ')
    const data = {
      firstName,
      lastName,
      email,
    }

    if (isVerfied == true) {
      contactUsFormValidation(data)
        .then(response => {
          console.log("Data Submitted Successfully", response)
          setsuccessSubmit('Your response is submitted..')
          emailResponse(response);
          //recaptchaInstance.reset();
          setFirstName('');
          setLastName('');
          setEmail('');
          setCaptchaText('');
          setFisrtNameVal('')
          setLastNameVal('')
          setEmailVal('')
        })
        .catch(err => {
          setFisrtNameVal(err.firstName)
          setLastNameVal(err.lastName)
          setEmailVal(err.email)
          console.log("Validation Err", err)
        })
    }
    else {
      setCaptchaText('Please, verify the captcha');
    }
  }

  const emailResponse = async (value) => {

    const res = await fetch('https://emailsender-shivendra.herokuapp.com/api/contact', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify(value)
    })
    const data = await res.json()
  }

  var callbackFooter = function () {
    console.log("Done!!!!")
  }


  var verifyCallbackFooter = function (response) {
    if (response) {
      checkVerified(true)
    }
  }

  //export const Footer = () => {
  return (


    <footer
      className="container-xxl pt-5 pb-4"
      style={{
        background: "url('https://res.cloudinary.com/espire/image/upload/v1620205113/samples/images/bg-home-bottom.png')",
      }}
    >

      <div className="row">
        <div className="container mt-lg-4">
          <div className="row">
            <div className="col-md-7">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-md-7">
                    <Logo class="footer-logo" />
                    <ContactDetail />
                    <ul className="list-inline d-flex footer-social justify-content-center justify-content-md-start">
                      <SocialMedia display="list-inline-item" />
                    </ul>
                  </div>
                  <div className="col-md-5">
                    <QuickLinks />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-5 d-none d-md-block">
              <h6>CONTACT US</h6>
              <form className="form">
                <div className="row no-gutters">
                  <div className="col-md-6">
                    <input
                      type="text"
                      className="pr-md-2"
                      placeholder="First Name"
                      name="fname"
                      id="fname"
                      aria-label="label"
                      title="title"
                      value={firstName}
                      onChange={(e) => setFirstName(e.target.value)}
                    />
                    <small>{fnameval}</small>
                  </div>

                  <div className="col-md-6">
                    <input
                      type="text"
                      placeholder="Last Name "
                      name="laname"
                      id="lname"
                      aria-label="label"
                      title="title"
                      value={lastName}
                      onChange={(e) => setLastName(e.target.value)}
                    />
                    <small>{lnameval}</small>
                  </div>

                  <div className="col-md-12">
                    <input
                      type="email"
                      placeholder="Email Address"
                      name="email"
                      id="email"
                      aria-label="label"
                      title="title"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                    />
                    <small>{emailval}</small>
                  </div>

                  <div className="col-md-12">
                    <Recaptchafooter
                      ref={e => (recaptchaInstancefooter = e)}
                      sitekey="6LcQRGAbAAAAAPq2xMD50I6tzOHekWer_x-r4lXz"
                      render="explicit"
                      verifyCallback={verifyCallbackFooter}
                      onloadCallback={callbackFooter}
                      elementID="g-recaptchanew"
                      onloadCallbackName="onloadCallback"
                      verifyCallbackName="verifyCallback"
                      expiredCallbackName="expiredCallback"
                      //render="onload"
                      theme="dark"
                      type="image"
                      size="normal"
                      tabindex="0"
                      hl="en"
                      badge="bottomright"
                    />
                    <span>{captchaText}</span>
                  </div>

                  <div className="col-md-12">
                    <button type="button"
                      style={{ background: "#9d6c26" }}
                      onClick={onSubmit} >
                      CONTACT US
                    </button>
                  </div>
                  <small>{successSubmit}</small>

                </div>
              </form>
              <ul className="list-inline">
                <CopyRight />
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>
  )
}
