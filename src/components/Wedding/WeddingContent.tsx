import React from "react"


export const WeddingContent: React.FC<any> = props => {

    const { allContentfulWeddingEvents } = props;
    const { iconImage } = allContentfulWeddingEvents.edges[0].node

    return (

        <div className="business-container bg-light-pink py-5 container-xxl">
            <section className="business-section mt-lg-4">
                <div className="container">
                    <div className="row justify-content-between">
                        {/* <div className="col-md-6 px-5" style={{ backgroundImage: `url(${allContentfulWeddingEvents.edges[0].node.backgroundImage.file.url})` }}> */}
                        <div className="col-md-6 " style={{ backgroundImage: "url('images/bg-home-bottom.png')" }}>
                            <div className="container text-center mt-5" >
                                <h1 className="yellow-text tk-gautreaux ">Your Wedding Planning</h1>
                                <h1 className="text-white tk-gautreaux ">With</h1>
                                <h1 className="text-white tk-gautreaux  ">Our Designed Packages </h1>
                                <img
                                    src={
                                        iconImage && iconImage.file.url
                                    }
                                    className="d-block mx-auto couple-icon"
                                    alt="img"
                                />
                            </div>
                        </div>
                        <div className="col-md-6 ">
                            <div className=" px-lg-5">
                                <h1 className="yellow-text-dark1 tk-gautreaux"> {allContentfulWeddingEvents.edges[0].node.title} </h1>
                                <p className="text-dark mt-3">
                                    {allContentfulWeddingEvents.edges[0].node.description.description}
                                </p>
                            </div>

                        </div>
                    </div>
                </div>

            </section>

        </div>



    )
}