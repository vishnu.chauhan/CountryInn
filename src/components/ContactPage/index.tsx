import React from "react"
import { Link, graphql } from "gatsby"
import { ContactCarousel } from "./ContactCarousel"
import { ContactDetail } from "./AddressDetails"

export const ContactPage: React.FC<any> = () => {
  return (
    <>
      <ContactDetail />
      <ContactCarousel />
    </>
  )
}
