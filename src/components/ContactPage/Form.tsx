import React from "react"
import { useState, useEffect, useCallback } from "react"
import { contactUsFormValidation } from "../Validations"

var Recaptcha = require("react-recaptcha")
let recaptchaInstance

// const mailgun = require("mailgun-js")
// const DOMAIN = "http://localhost:8000"
// const mg = mailgun({
//   apiKey: "92219cb4ff349d615512d33092b3e877-602cc1bf-8e449839",
//   domain: "DOMAIN",
// })

export const ContactForm: React.FC<any> = () => {
  const [firstName, setFirstName] = useState("")
  const [lastName, setLastName] = useState("")
  const [email, setEmail] = useState("")
  const [errors, setErrors] = useState({})
  const [fnameval, setFisrtNameVal] = useState('')
  const [lnameval, setLastNameVal] = useState('')
  const [emailval, setEmailVal] = useState('')
  const [isVerfied, checkVerified] = useState(false);
  const [captchaText, setCaptchaText] = useState('');
  const [successSubmit, setsuccessSubmit] = useState('');


  const handleSubmit = evt => {
    evt.preventDefault()
    setsuccessSubmit('')
    // const data = {
    //   from: "vishnu.chauhan@espire.com",
    //   to: "vishnu.chauhan@espire.com",
    //   subject: "Hello",
    //   text: "Testing some Mailgun awesomness!",
    // }
    // mg.messages().send(data, function (error, body) {
    //   console.log(body)
    // })

    var data = {
      firstName,
      lastName,
      email,
    }
    if (isVerfied == true) {
      contactUsFormValidation(data)
        .then(response => {
          console.log("Data Submitted Successfully", response)
          emailResponse(response)
          setsuccessSubmit('Your response is submitted..')
          //recaptchaInstance.reset();
          setFirstName('');
          setLastName('');
          setEmail('');
          setCaptchaText('');
          setFisrtNameVal('')
          setLastNameVal('')
          setEmailVal('')
        })
        .catch(err => {
          setFisrtNameVal(err.firstName)
          setLastNameVal(err.lastName)
          setEmailVal(err.email)
          console.log("Validation Err", err)
        })

    }
    else {
      setCaptchaText('Please, verify the captcha');
    }
  }


  var callback = function () {
    console.log("Done!!!!")
  }

  var verifyCallback = function (response) {
    //console.log("sdssssssssss", response)
    if (response) {
      checkVerified(true)
    }
  }

  const emailResponse = async (value) => {

    const res = await fetch('https://emailsender-shivendra.herokuapp.com/api/contact', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify(value)
    })
    const data = await res.json()
  }

  return (
    <>
      <div>
        <div className="col-md-8 offset-md-2">
          <h5 className="text-white mt-md-5">Contact Us</h5>
          <form className="form">
            <div className="row no-gutters">
              <div className="col-md-6">
                <input
                  type="text"
                  className="pr-md-2"
                  placeholder="First Name"
                  name="fname"
                  id="fname"
                  aria-label="label"
                  title="title"
                  value={firstName}
                  onChange={e => setFirstName(e.target.value)}
                />
                <small>{fnameval}</small>
                {/* <div className="invalid-tooltip">{fnameval}</div> */}
                {/* {errors && errors.firstName && (
                  <div className="invalid-tooltip">{errors.firstName}</div>
                )} */}
              </div>
              <div className="col-md-6">
                <input
                  type="text"
                  placeholder="Last Name "
                  value={lastName}
                  onChange={e => setLastName(e.target.value)}
                  name="lname"
                  id="lname"
                  aria-label="label"
                  title="title"
                />
                <small>{lnameval}</small>
                {/* <div className="invalid-tooltip">{lnameval}</div> */}
                {/* {errors && errors.lastName && (
                  <div className="invalid-tooltip">{errors.lastName} </div>
                )} */}
              </div>
              <div className="col-md-12">
                <input
                  type="email"
                  value={email}
                  placeholder="Email Address"
                  onChange={e => setEmail(e.target.value)}
                  name="email"
                  id="email"
                  aria-label="label"
                  title="title"
                />
                <small>{emailval}</small>
                {/* <div className="invalid-tooltip">{emailval}</div> */}
                {/* {errors && errors.email && (
                  <div className="invalid-tooltip">{errors.email} </div>
                )} */}
              </div>
              <div className="col-md-12">
                <div className="px-auto">
                  <Recaptcha
                    ref={e => (recaptchaInstance = e)}
                    sitekey="6LcQRGAbAAAAAPq2xMD50I6tzOHekWer_x-r4lXz"
                    //sitekey="6LeDbtEaAAAAAHCT9jSddPPigQ8sbVARf0w9GbIY"
                    render="explicit"
                    verifyCallback={verifyCallback}
                    onloadCallback={callback}
                    // theme="dark"
                    // type="image"

                    //elementID="g-recaptcha"
                    onloadCallbackName="onloadCallback"
                    verifyCallbackName="verifyCallback"
                    expiredCallbackName="expiredCallback"
                    //render="onload"
                    theme="dark"
                    type="image"
                    size="normal"
                    tabindex="0"
                    hl="en"
                    badge="bottomright"
                  />


                </div>

              </div>
              {/* <Recaptcha
                ref={e => (recaptchaInstance = e)}
                sitekey="6LcQRGAbAAAAAPq2xMD50I6tzOHekWer_x-r4lXz"
                //sitekey="6LeDbtEaAAAAAHCT9jSddPPigQ8sbVARf0w9GbIY"
                render="explicit"
                verifyCallback={verifyCallback}
                onloadCallback={callback}
                // theme="dark"
                // type="image"

                //elementID="g-recaptcha"
                onloadCallbackName="onloadCallback"
                verifyCallbackName="verifyCallback"
                expiredCallbackName="expiredCallback"
                //render="onload"
                theme="dark"
                type="image"
                size="normal"
                tabindex="0"
                hl="en"
                badge="bottomright"
              /> */}
              <small>{captchaText}</small>


              <div className="col-md-12">
                <button
                  type="submit"
                  title="title"
                  aria-label="aria-label"
                  name="btn"
                  onClick={handleSubmit}
                >
                  CONTACT US
                </button>
              </div>
              <small>{successSubmit}</small>
            </div>
          </form>
        </div>
      </div>
    </>
  )
}
