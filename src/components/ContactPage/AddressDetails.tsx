import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"
import { ContactForm } from "./Form"

export const ContactDetail: React.FC<any> = props => {
  return (
    <StaticQuery
      query={graphql`
        query contactDetails {
          contentfulContactAddress {
            title
            subTitle
            description {
              description
            }
            emailText
            phoneText
            addressText
            address {
              address
            }
            email
            phone
            alternatePhone
            countryCode
            cityCode
          }
        }
      `}
      render={data => (
        <>
          <div
            className="container-xxl position-relative contact-our-address"
            style={{
              background: "url('images/carousel-img.png')",
            }}
          >
            <div className="container pt-5 pb-4">
              <div className="row">
                <div className="col-lg-8 offset-lg-2">
                  <h1 className="yellow-text text-center mt-lg-4 tk-gautreaux">
                    {data.contentfulContactAddress &&
                      data.contentfulContactAddress.title}
                    &nbsp;
                    <span className="text-white">
                      {data.contentfulContactAddress &&
                        data.contentfulContactAddress.subTitle}
                    </span>
                  </h1>
                  <div className="text-white text-center ls-2 font-weight-200">
                    <p>
                      {data.contentfulContactAddress &&
                        data.contentfulContactAddress.description &&
                        data.contentfulContactAddress.description.description}
                    </p>
                  </div>
                  <ContactForm />
                </div>
              </div>
            </div>
          </div>

          <div className="container-xxl">
            <div className="row">
              <ul className="contact-nav mb-0 bg-secondary w-100 d-flex justify-content-around">
                <li className="px-3 py-4 p-md-4 p-lg-4">
                  <div>
                    <span className="fa fa-div fa-address-card"></span>
                  </div>
                  <div>
                    <h5 className="text-uppercase ls-2 font-weight-light">
                      {data.contentfulContactAddress &&
                        data.contentfulContactAddress.addressText}
                    </h5>
                    <p>
                      {data.contentfulContactAddress &&
                        data.contentfulContactAddress.address.address}
                    </p>
                  </div>
                </li>
                <li className="px-3 py-4 p-md-4 p-lg-4">
                  <div>
                    <span className="fa fa-div fa-envelope"></span>
                  </div>
                  <div>
                    <h5 className="text-uppercase ls-2 font-weight-light">
                      {data.contentfulContactAddress &&
                        data.contentfulContactAddress.emailText}
                    </h5>
                    <p>
                      <a
                        className="text-white"
                        href={`mailto:${data.contentfulContactAddress &&
                          data.contentfulContactAddress.email
                          }`}
                      >
                        {data.contentfulContactAddress &&
                          data.contentfulContactAddress.email}
                      </a>
                    </p>
                  </div>
                </li>
                <li className="px-3 py-4 p-md-4 p-lg-4">
                  <div>
                    <span className="fa fa-div fa-mobile"></span>
                  </div>
                  <div>
                    <h5 className="text-uppercase ls-2 font-weight-light">
                      {data.contentfulContactAddress &&
                        data.contentfulContactAddress.phoneText}
                    </h5>
                    <p>
                      +
                      {data.contentfulContactAddress &&
                        data.contentfulContactAddress.cityCode}
                      -
                      {data.contentfulContactAddress &&
                        data.contentfulContactAddress.alternatePhone}
                      , +
                      {data.contentfulContactAddress &&
                        data.contentfulContactAddress.countryCode}
                      -
                      {data.contentfulContactAddress &&
                        data.contentfulContactAddress.phone}
                    </p>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </>
      )}
    />
  )
}
