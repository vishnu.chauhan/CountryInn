import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const ContactCarousel: React.FC<any> = props => {
  return (
    <StaticQuery
      query={graphql`
        query homeCaurodsal {
          contentfulDestinationsCarousel(slug: { eq: "Contact" }) {
            title
            subTitle
            slug
            name
            description {
              description
            }
            destination {
              title
              subTitle
              slug
              address {
                address
              }
              addressLocation {
                lat
                lon
              }
              alternateContactNo
              contactNo
              countryCode
              image {
                title
                file {
                  url
                }
              }
            }
          }
        }
      `}
      render={data => (
        <>
          <div className="container-xxl contact-hr">
            <div className="row  py-5">
              <div className="container">
                <h1 className=" text-center mt-lg-4 tk-gautreaux">
                  {data.contentfulDestinationsCarousel.title} -{" "}
                  {data.contentfulDestinationsCarousel.subTitle}
                </h1>
                <div className=" mt-5">
                  <ul className="owl-carousel contact_h_r_owl">
                    {data.contentfulDestinationsCarousel.destination.map(
                      (item, key) => (
                        <li style={{ height: "260px" }} key={key}>
                          <img
                            className="imgsize"
                            src={`${item &&
                              item.image &&
                              item.image.file &&
                              item.image.file.url
                              }`}
                            alt="img"
                          />
                          <div className="content">
                            <h5 className="text-uppercase mb-0 text-white">
                              {item.title}
                            </h5>
                            <p className="mb-0 mt-3 font-weight-200 px-5 text-white">
                              {item && item.address && item.address.address}
                              <br /> +{item && item.countryCode} -{" "}
                              {item && item.contactNo}
                              <br /> +{item && item.countryCode} -{" "}
                              {item && item.alternateContactNo}
                            </p>
                            <Link
                              to={`https://www.google.com/maps/search/?api=1&query=${item &&
                                item.addressLocation &&
                                item.addressLocation.lat
                                },${item &&
                                item.addressLocation &&
                                item.addressLocation.lon
                                }`}
                              target="_blank"
                              className="book-button"
                            >
                              <span className="d-inline-block py-2 pl-3 pr-2">
                                Directions
                              </span>
                              <span className="d-inline-block p-3 bg-yellow-light">
                                <i
                                  className="fa fa-send"
                                  aria-hidden="true"
                                ></i>
                              </span>
                            </Link>
                          </div>
                        </li>
                      )
                    )}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    />
  )
}
