import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const TermsConditionContent: React.FC<any> = props => {

    const { contentfulTermsConditions } = props;

    return (

        <div className="container-xxl py-5 hotals-tabs term-condition">
            <div className="row pt-5">
                <div className="container">
                    <article className="tabbed-content">
                        <nav className="tabs">
                            <ul className="list-inline d-flex">
                                <li><a href="#tab1" className="active">{contentfulTermsConditions.termAndConditionTitle}</a></li>
                                <li><a href="#tab2">{contentfulTermsConditions.privacyPolicyTitle}</a></li>
                                <li><a href="#tab3">{contentfulTermsConditions.refundCancellationPolicyTitle}</a></li>
                                <li><a href="#tab4">{contentfulTermsConditions.childPolicyTitle}</a></li>
                            </ul>
                        </nav>
                        <div id="tab1" className="item active" data-title="Terms & Conditions">
                            <div className="container-fluid item-content">
                                <h1 className="yellow-text-dark tk-gautreaux">{contentfulTermsConditions.termAndConditionTitle}</h1>
                                <p>
                                    {contentfulTermsConditions.termsAndConditionContentPara1.termsAndConditionContentPara1}
                                </p>
                                <p>{contentfulTermsConditions.termsAndConditionContentPara2.termsAndConditionContentPara2} </p>

                                <p>
                                    {contentfulTermsConditions.termsAndConditionContentPara3.termsAndConditionContentPara3}
                                </p>
                                <p> {contentfulTermsConditions.termsAndConditionContentPara4.termsAndConditionContentPara4}
                                </p>
                            </div>
                        </div>
                        <div id="tab2" className="item" data-title="Privacy Policy">
                            <div className="container-fluid item-content">
                                <h1 className="yellow-text-dark tk-gautreaux">{contentfulTermsConditions.privacyPolicyTitle}</h1>
                                <p> {contentfulTermsConditions.privacyPolicyContentPara1.privacyPolicyContentPara1}</p>
                                <p>{contentfulTermsConditions.privacyPolicyContentPara2.privacyPolicyContentPara2} </p>

                            </div>
                        </div>
                        <div id="tab3" className="item" data-title="Refund & Cancellation Policy">
                            <div className="container-fluid item-content">
                                <h1 className="yellow-text-dark tk-gautreaux">{contentfulTermsConditions.refundCancellationPolicyTitle}</h1>
                                <ul className="text-dark">
                                    {contentfulTermsConditions.refundCancellationPolicyList.map((item, index) => (
                                        <li key={index}> <i className="fa fa-check"></i> {item}</li>
                                    ))}

                                </ul>
                            </div>
                        </div>
                        <div id="tab4" className="item" data-title="Child Policy">
                            <div className="container-fluid item-content">
                                <h1 className="yellow-text-dark tk-gautreaux">{contentfulTermsConditions.childPolicyTitle}</h1>
                                <p>{contentfulTermsConditions.childPolicyContent.childPolicyContent}
                                </p>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>



    )
}