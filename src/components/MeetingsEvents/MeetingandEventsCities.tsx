import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"
import { data } from "jquery"

export const MeetingandEventsCities: React.FC<any> = props => {

    const { cities } = props.allContentfulMeetingAndEvents.edges[0].node

    //console.log(cities)

    let newCities = cities.map((item, index) => {
        return {
            item: item,
            index: index
        }
    })

    //console.log(newCities)


    const checkCities = (value) => {

        if (value.index % 2 == 0)
            return (
                <div className="py-5 bg-light-pink">
                    <div className="container ">
                        <div className="row ">
                            <div className="col-lg-9 p-0">
                                <div className="item-content">
                                    <div className="htl-img">
                                        <img src={`${value.item.image.file.url}`}></img>
                                    </div>
                                    <div className="htl-content  transparent-content">
                                        <div className="py-5 px-3 px-lg-5">
                                            <h1 className="tk-gautreaux yellow-text ">{value.item.name}
                                            </h1>
                                            <div className="mt-md-4 mt-2">
                                                <p>{value.item.description}</p>
                                            </div>
                                            <Link
                                                to={value.item.slug}
                                                className=" book-button"
                                            >
                                                <span className="d-inline-block py-2 py-lg-3 pl-3 pr-2">Send Query</span>
                                                <span className="d-inline-block p-3 py-lg-3 bg-yellow-light">
                                                    <i className="fa fa-angle-double-right" aria-hidden="true"></i></span>
                                            </Link>
                                            {/* <a href={value.item.slug} className="book-button">
                                                <span className="d-inline-block py-2 py-lg-3 pl-3 pr-2">EXPLORE</span>
                                                <span className="d-inline-block p-3 py-lg-3 bg-yellow-light"><i
                                                    className="fa fa-angle-double-right" aria-hidden="true"></i></span>
                                            </a> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            )
        else
            return (
                <div className="py-5 bg-dark-pink ">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-3 p-0"></div>
                            <div className="col-lg-9 p-0">
                                <div className="item-content">
                                    <div className="htl-img">
                                        <img src={`${value.item.image.file.url}`} />
                                    </div>
                                    <div className="htl-content left-content transparent-content">
                                        <div className="py-5 px-3 px-lg-5">
                                            <h1 className="tk-gautreaux yellow-text ">{value.item.name}
                                            </h1>
                                            <div className="mt-md-4 mt-2">
                                                <p>{value.item.description}</p>
                                            </div>
                                            <Link
                                                to={value.item.slug}
                                                className=" book-button"
                                            >
                                                <span className="d-inline-block py-2 py-lg-3 pl-3 pr-2">Send Query</span>
                                                <span className="d-inline-block p-3 py-lg-3 bg-yellow-light">
                                                    <i className="fa fa-angle-double-right" aria-hidden="true"></i></span>
                                            </Link>
                                            {/* <a href={value.item.slug} className="book-button">
                                                <span className="d-inline-block py-2 py-lg-3 pl-3 pr-2">Explore</span>
                                                <span className="d-inline-block p-3 py-lg-3 bg-yellow-light"><i
                                                    className="fa fa-angle-double-right" aria-hidden="true"></i></span>
                                            </a> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            )

    }
    return (

        <section>
            {newCities.map((item, index) => (
                checkCities(item)
            ))}
        </section>

    )
}