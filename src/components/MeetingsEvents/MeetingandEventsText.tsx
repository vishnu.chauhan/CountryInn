import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const MeetingandEventsText: React.FC<any> = props => {

    const { allContentfulMeetingAndEvents } = props;

    return (


        // <div className="main">
        //     <div className="slider slider-for-img">
        //         <div className="position-relative">
        //             <img
        //                 src={`${data.contentfulMeetingAndEvents.backgroundImage.file.url}`}
        //                 className="d-block w-100"
        //                 alt="Img"
        //             />
        //             <div className="carousel-caption py-5">
        //                 <h3 className="tk-gautreaux py-5">
        //                     M.C.E.N
        //                     <br /><span className="  yellow-text  ">
        //                         {data.contentfulMeetingAndEvents.shortDescription.shortDescription} </span>
        //                 </h3>
        //                 <p className="text-uppercase py-4">
        //                     {data.contentfulMeetingAndEvents.description &&
        //                         data.contentfulMeetingAndEvents.description.description}
        //                 </p>
        //             </div>
        //         </div>
        //     </div>
        // </div>


        <section style={{ backgroundImage: "url('./images/carousel-img.png')" }} className="container-xxl meeting-content">
            <div className="container">
                <div className="row">
                    <div className="col-md-10 offset-md-1 py-5 text-center">
                        <div className="my-lg-5">
                            <h1 className="tk-gautreaux font-weight-bold yellow-text">M.I.C.E.</h1>
                            <h1 className="tk-gautreaux  yellow-text">{allContentfulMeetingAndEvents.edges[0].node.shortDescription.shortDescription} <br /> {allContentfulMeetingAndEvents.edges[0].node.shortDescriptionLineSecond}
                            </h1>
                            <h5 className="text-white font-weight-light mt-4">{allContentfulMeetingAndEvents.edges[0].node.aboveDescription.aboveDescription}</h5>
                            <div className="px-lg-5 mt-4">
                                <p className="text-white mt-4">{allContentfulMeetingAndEvents.edges[0].node.description.description}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    )
}