import * as React from "react"
import { graphql } from "gatsby"
import { Layout } from "../../layouts/default"

const NotFoundPage = ({}) => {
  return (
    <Layout>
      <h1>404: Not Found</h1>
      <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
    </Layout>
  )
}

export default NotFoundPage
