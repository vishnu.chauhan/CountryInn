import { Link, graphql } from "gatsby"
import React, { Component } from "react"

export const Experience: React.FC<any> = ({
  title,
  description,
  experience,
  displaySection,
}) => {

  return (
    <>
      <div className="expariences" key={title}>
        <div
          className={`${displaySection == "Light"
            ? "py-5 bg-light-pink"
            : "py-5 bg-dark-pink"
            }`}
        >
          <div className="container">
            <h1
              className=" mt-lg-4 mt-3 mb-3 yellow-text-dark1 font-weight-700
                 tk-gautreaux"
            >
              {title}
            </h1>
            <div>
              <p>{description.description}</p>
            </div>

            <div className="owl-carousel expariences-owl">
              {experience.map((item, key) => (
                <div key={key}>
                  <div className="e-owl-grid">
                    <img
                      src={
                        item &&
                        item.image &&
                        item.image.file &&
                        item.image.file.url
                      }
                      alt="image"
                    />
                    <div className="e-owl-text">
                      <p>{item.subTitle}</p>
                      <h5>{item.title}</h5>
                    </div>
                  </div>
                </div>
              ))}
            </div>
            <Link to="#" className="book-button position-relative mt-4">
              <span className="d-inline-block text-uppercase py-2 py-lg-3 pl-3 pr-3">
                Explore All
              </span>
              <span className="d-inline-block px-3 py-3 bg-yellow-light">
                <i className="fa fa-angle-double-right" aria-hidden="true"></i>
              </span>
            </Link>
          </div>
        </div>
      </div>
    </>
  )
}

export const query = graphql`
  fragment experience on ContentfulExperience {
    sys {
      contentType {
        sys {
          id
          linkType
          type
        }
      }
      revision
      type
    }
    title
    displaySection
    description {
      description
    }
    experience {
      title
      subTitle
      slug
      image {
        file {
          url
        }
      }
    }
  }
`
