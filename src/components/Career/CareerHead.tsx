import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"
import { ContactDetailleft } from "../Header/ContactDetailleft"
import { BookNow } from "../Header/BookNow"
import { BookNowMobile } from "../Header/BookNowMobile"
import { SocialMedia } from "../Header/SocialMedia"
import { BannerImageName } from "../Header/BannerImageName"


export const CareerHead: React.FC<any> = props => {

    const { contentfulCareerPage } = props;

    return (
        <>
            <div className="container-xxl  position-relative ">
                <div className="row">
                    <div className="home-page-banner inner-page-banner">
                        <div id="Carousel">
                            <div className="main">
                                <div className="slider slider-for-img">
                                    <div className="position-relative">
                                        <img
                                            src={`${contentfulCareerPage.imageCareerBanner.file.url}`}
                                            //src="./images/hero-banner.jpg"
                                            className="d-block w-100"
                                            alt="Img"
                                        />
                                        <div className="carousel-caption text-left pl-4">
                                            <h3 className="tk-gautreaux yellow-text">
                                                {/* {contentfulCareerPage.bannerHeading}{" "} */}
                                                <span className="  yellow-text ">{contentfulCareerPage.bannerHeading}</span>
                                            </h3>
                                            <p className="text-uppercase">
                                                {/* {item.node.intro && item.node.intro.intro} */}
                                            </p>
                                        </div>
                                    </div>
                                    {/* ))} */}
                                </div>
                            </div>

                            <BookNow />
                            <ContactDetailleft />
                            <div className="d-none d-md-flex right-black-strip">
                                <div >
                                    <span className="strip-gray"></span>
                                    <a href="" className="text-uppercase">
                                        {" "}<i className="fa fa-map-marker"></i> {contentfulCareerPage.careerPathHeading}</a>
                                </div>
                                <div>
                                    <ul className="list-in d-block  sidebar-social-media">
                                        <SocialMedia disaply="list-inline-items" />
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <BookNowMobile />

        </>

    )
}