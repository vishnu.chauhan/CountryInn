import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const CareerListContent: React.FC<any> = props => {

    const { contentfulCareerPage } = props;

    return (

        <section className="container-xxl py-5 hotals-tabs term-condition">
            <div className="row ">
                <div className="container">
                    <h3 className="yellow-text-dark tk-gautreaux mt-4 mt-lg-2">{contentfulCareerPage.careerListHeading}</h3>
                    <ul className="mt-3 text-dark">
                        {contentfulCareerPage.careerList.map((item, index) => (
                            <li key={index}> <i className="fa fa-check"></i>{item}</li>
                        ))}

                    </ul>
                    <p className="text-14 text-dark">
                        {contentfulCareerPage.emailToHeading} <a className="yellow-text-dark"
                            href="mailto:hr@countryinn.in">{contentfulCareerPage.emailid}</a></p>
                </div>
            </div>
        </section>
    )
}