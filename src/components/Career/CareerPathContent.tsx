import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"


export const CareerPathContent: React.FC<any> = props => {

    const { contentfulCareerPage } = props;

    return (

        <section className="container-xxl  bg-dark-pink">
            <div className="container">
                <div className="row">
                    <div className="col-md-12 py-5 ">
                        <div className="my-lg-4">
                            <h1 className="tk-gautreaux text-center font-weight-bold yellow-text-dark">{contentfulCareerPage.careerPathHeading}
                            </h1>
                            <div className="mt-4 text-dark">
                                <p className=" mt-4">{contentfulCareerPage.careerPathContentPara1.careerPathContentPara1}</p>
                                <p className=" mt-4">{contentfulCareerPage.careerPathContentPara2.careerPathContentPara2}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}