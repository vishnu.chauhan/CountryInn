import React from "react"
import { useState } from "react"

export const BookNowMobile: React.FC<any> = ({ title }) => {
  const [action, setAction] = useState(
    "https://linkprotect.cudasvc.com/url?a=https%3a%2f%2fhotels.eglobe-solutions.com%2fcountryinn%2fBooking%2fDetail%2f12203%23bookingsteps&c=E,1,BCwiZd2pGMHWjRqJMFYgtO-Qb6W7Zpo2055YBf7JpLRTVoOjZrBgR-qE3zjofmDNFf2JQUY4cLxVyPXGWQrWQDpkcA52u5sx3-FwXmkUIBHzD6bimUDFpf4ytt5R&typo=1"
  )

  const [startDate, setStartDate] = useState(new Date())
  const [endDate, setEndDate] = useState(new Date())
  const [staydays, setStayDays] = useState(1)

  const handleSubmit = evt => {
    let checkindate = $("#datepicker-in").val()
    let checkoutdate = $("#datepicker-out").val()

    if (checkindate != "" && checkoutdate != "") {
      var chkindt = new Date(checkindate.toString())
      var chkoutdt = new Date(checkoutdate.toString())
      var timeDiff = chkoutdt.getTime() - chkindt.getTime()
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24))
      if (diffDays == 0) {
        diffDays = diffDays + 1
      }
      if (diffDays > 0) {
        setStayDays(diffDays)
      }
      window.location.href = action
    }
  }
  return (
    <div className="container-fluid mobile-book-now d-md-none">
      <div className="row">
        <div className=" col-12">
          <div className="mt-2 pt-1 ">
            <span className="font-weight-500 text-body">DESTINATIONS</span>
            <div className=" mt-1 position-relative ">
              <div className="book-icon">
                <img src="https://res.cloudinary.com/espire/image/upload/v1620205095/samples/images/map-icon.svg" />
              </div>
              {/* <div className="book-icon">
                <img
                  src="https://res.cloudinary.com/espire/image/upload/v1620205095/samples/images/map-icon.svg"
                  alt="img"
                /> 
              </div> */}
              <select
                onChange={e => setAction(e.target.value)}
                name="cbohotels2"
                aria-label="label"
                title="select"
                style={{ width: "100%" }}
              >
                <option value="" aria-label="label" title="title">
                  Select City
                </option>
                <option value="https://linkprotect.cudasvc.com/url?a=https%3a%2f%2fhotels.eglobe-solutions.com%2fcountryinn%2fBooking%2fDetail%2f12203%23bookingsteps&c=E,1,lj1qRI8_mKdULAWaeNNGvA9u--wMH9k4Z-UxV7H3niGgXjCF7bLTi33CQ_HL_Q0Kmu2uQkSAbmvek7mxh6PAXILBg-C8KxddQq87-a7YjUy8ziI,&typo=1">
                  Amritsar
                </option>
                <option value="https://linkprotect.cudasvc.com/url?a=https%3a%2f%2fhotels.eglobe-solutions.com%2fcountryinn%2fBooking%2fDetail%2f272%23bookingsteps&c=E,1,knD71UFg63jtbKBpeCVzrjD0Z2SBlgWnvvVAUXpGcJ2Uzlv1Ldx1wbgrl4Hf9UF05ek9HTvRP3-bSzpAvPuPGLJauwgcnZbSH6Rw5rNDF8FFqQ,,&typo=1">
                  Bhimtal
                </option>
                <option value="https://linkprotect.cudasvc.com/url?a=https%3a%2f%2fhotels.eglobe-solutions.com%2fcountryinn%2fBooking%2fDetail%2f15062%23bookingsteps&c=E,1,3u_2xiJKJpsWQ8rQlXFPJendwYAcb-MNyKX-ViVT6BG-GTlloNwwsR_uNnhY_aGICgLKDLj2vl8yFITTTNfXzx37tBtT-u_O01eClopgFj6n&typo=1">
                  Budaun
                </option>
                <option value="https://linkprotect.cudasvc.com/url?a=https%3a%2f%2fhotels.eglobe-solutions.com%2fcountryinn%2fBooking%2fDetail%2f269%23bookingsteps&c=E,1,QgJ_wvxYxGpXyURPLCzj62d5a-H_FSA-A7v-vdZcQlfounWJnl6BfJXwx7XWL0yMH4DfBxUvWZ_rFqv3Fxnas2ddWQu9Ysy3iiLHgNsI9KCV&typo=1">
                  Corbett
                </option>

                <option value="https://linkprotect.cudasvc.com/url?a=https%3a%2f%2fhotels.eglobe-solutions.com%2fcountryinnvrindavan%2fBooking%2fDetail%23bookingsteps&c=E,1,b5fzzuMeUn3IAzgHN6xosDc2XDm54pLcSwC2_ufuq_OPyHU7_zl-zNtHCf3RJpLJNo_Rljuw7PMHAIFekwUelA4FjSNIa_S1KXXqeeRCpOAFZ9NRMdU,&typo=1">
                  Vrindavan
                </option>
                <option value="https://linkprotect.cudasvc.com/url?a=https%3a%2f%2fhotels.eglobe-solutions.com%2fcountryinnharidwar%2fBooking%2fDetail%23bookingsteps&c=E,1,9opQqKNAZGa1jXVwCgTLt9IVYLlw_VHNBI70_PcBmgfqXDrAw3giBpo3xc2JRfaDjPQSCPcysZhHYZAtLFH9gQSqC6KLrszRYHI8gtDKED0L7dWbRIl-1YvR&typo=1">
                  Haridwar
                </option>
              </select>
            </div>
          </div>
        </div>

        <div className=" col-6">
          <div className="mt-2 pt-1 ">
            <span className="font-weight-500 text-body">CHECK - IN</span>
            <div className=" mt-1 position-relative">
              <div className="book-icon">
                <img
                  src="https://res.cloudinary.com/espire/image/upload/v1620205104/samples/images/calendar-icon.svg"
                  alt="image"
                />
              </div>
              <input
                id="datepicker-in-m"
                readOnly
                type="text"
                placeholder="Select Date"
                title="title"
                aria-label="label"
                name="checkInDateParam-m"
                className="date"
                defaultValue={startDate.toLocaleDateString()}
                value={startDate.toLocaleDateString()}
                onChange={e => {
                  setStartDate(new Date(e.target.value))
                }}
              />
            </div>
          </div>
        </div>

        <div className="col-md-3  col-6">
          <div className="mt-2 pt-1 ">
            <span className="font-weight-500 text-body">CHECK - OUT</span>
            <div className=" mt-1 position-relative">
              <div className="book-icon">
                <img
                  src="https://res.cloudinary.com/espire/image/upload/v1620205104/samples/images/calendar-icon.svg"
                  alt="image"
                />
              </div>
              <input
                id="datepicker-out-m"
                readOnly
                type="text"
                placeholder="Select Date"
                name="ch_out-m"
                title="title"
                aria-label="label"
                defaultValue={endDate.toLocaleDateString()}
                value={endDate.toLocaleDateString()}
                onChange={e => {
                  setEndDate(new Date(e.target.value))
                }}
                className="date"
              />
            </div>
          </div>
        </div>
        <input
          type="hidden"
          name="numNightsParam-m"
          id="numNightsParam-m"
          value="1"
          title="title"
          aria-label="label"
        />
        <div className="col-md-3 my-3 col-12">
          <button
            className="book-now-mbl-btn text-center text-white d-block"
            onClick={handleSubmit}
            name="bootnow"
            title="title"
            aria-label="label"
            style={{ border: "none", width: "100%", cursor: "pointer" }}
          >
            BOOK ONLINE<span className="d-none d-lg-block">ONLINE</span>
          </button>
        </div>
      </div>
    </div>
  )
}
