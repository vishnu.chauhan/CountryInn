import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const SocialMedia: React.FC<any> = props => {
  return (
    <StaticQuery
      query={graphql`
        query MyQuery {
          allContentfulSocialMedia {
            edges {
              node {
                name
                icon
                url
              }
            }
          }
        }
      `}
      render={data => (
        <>
          {data.allContentfulSocialMedia.edges.map((item, key) => (
            <li className={`${props && props.display}`} key={key}>
              <Link
                to={item.node.url}
                id={`social-${key}${props.display}`}
                className="social"
              >
                <span className="sr-only">socal</span>
                <i className={`fa ${item.node.icon}`}></i>
              </Link>
            </li>
          ))}
        </>
      )}
    />
  )
}
