import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const Navbar: React.FC<any> = props => {
  return (
    <StaticQuery
      query={graphql`
        query Nav {
          contentfulNavigation {
            navItems {
              slug
              name
              menuItems {
                ... on ContentfulDestination {
                  id
                  title
                  slug
                  bookNowLink
                  quickPayLink
                }
                ... on ContentfulGallery {
                  id
                  title
                  slug
                }
                ... on ContentfulPlanAnEventMenuItem{
                  name
                  slug
                }
              }
            }
          }
        }
      `}
      render={data => (
        <>
          <div className="menu-panel">
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-2 col-2">
                  <button className="panel-menu-button bg-black text-white">
                    <i
                      className="fa mb-2 d-block fa-times"
                      aria-hidden="true"
                    ></i>
                    &nbsp;
                    <small>MENU</small>
                  </button>
                </div>
                <div className="col-md-8 vh-100 col-10">
                  <div className="mt-2 mt-md-5">
                    <a href="/">
                      <img
                        className="logo logo-menu-panel"
                        src="https://res.cloudinary.com/espire/image/upload/v1620205095/samples/images/logo.png"
                        alt="img"
                      />
                    </a>
                  </div>

                  <ul className="list-inline mt-4 full-menu pl-3">
                    {data.contentfulNavigation.navItems.map((item, key) => (
                      <li key={key}>
                        <Link
                          to={`${item && item.menuItems ? "#" : item.slug}`}
                          className={`${item && item.menuItems ? "sub-menu-link" : ""
                            }`}
                        >
                          {item.name}
                        </Link>
                        {item && item.menuItems && item.name !== 'Packages' && item.name !== 'quick pay' && item.name !== 'plan an event' && (
                          <ul className="sub-menu pl-4 pt-md-5 pt-1 pl-md-5">
                            {item.menuItems.map((pageitemData, pageIndex) => {
                              return (
                                <li key={pageIndex}>
                                  <Link
                                    to={`/${item.slug}/${pageitemData.slug}`}
                                  >
                                    {pageitemData.title}
                                  </Link>
                                </li>
                              )
                            })}
                          </ul>
                        )}
                        {item && item.menuItems && item.name === 'Packages' && (
                          <ul className="sub-menu pl-4 pt-md-5 pt-1 pl-md-5">
                            {item.menuItems.map((pageitemData, pageIndex) => {
                              return (
                                <li key={pageIndex}>
                                  <Link
                                    to={`${pageitemData.bookNowLink}`}
                                  >
                                    {pageitemData.title}
                                  </Link>
                                </li>
                              )
                            })}
                          </ul>
                        )}
                        {item && item.menuItems && item.name === 'quick pay' && (
                          <ul className="sub-menu pl-4 pt-md-5 pt-1 pl-md-5">
                            {item.menuItems.map((pageitemData, pageIndex) => {
                              return (
                                <li key={pageIndex}>
                                  <Link
                                    to={`${pageitemData.quickPayLink}`}
                                  >
                                    {pageitemData.title}
                                  </Link>
                                </li>
                              )
                            })}
                          </ul>
                        )}
                        {item && item.menuItems && item.name === 'plan an event' && (
                          <ul className="sub-menu pl-4 pt-md-5 pt-1 pl-md-5">
                            {item.menuItems.map((pageitemData, pageIndex) => {
                              return (
                                <li key={pageIndex}>
                                  <Link
                                    to={`${pageitemData.slug}`}
                                  >
                                    {pageitemData.name}
                                  </Link>
                                </li>
                              )
                            })}
                          </ul>
                        )}
                      </li>
                    ))}
                  </ul>
                </div>

                <div className="col-md-2 d-none d-md-block"></div>
              </div>
            </div>
          </div>
        </>
      )}
    />
  )
}
