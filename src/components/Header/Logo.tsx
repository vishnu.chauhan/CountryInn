import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const Logo: React.FC<any> = props => {
  return (
    <StaticQuery
      query={graphql`
        query logo {
          contentfulLogo {
            image {
              file {
                url
              }
            }
          }
        }
      `}
      render={data => (
        <img
          className={`${props && props.class}`}
          src={data.contentfulLogo.image.file.url}
          alt="img"
        />
      )}
    />
  )
}
