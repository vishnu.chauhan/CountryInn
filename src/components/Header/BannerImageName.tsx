import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"
import { globalHistory } from "@reach/router"

export const BannerImageName: React.FC<any> = props => {
  return (
    <StaticQuery
      query={graphql`
        query pageBannerImageName {
          contentfulPageBanner(slug: { eq: "Home" }) {
            bannerItems {
              imageDetail
            }
          }
        }
      `}
      render={data => (
        <div className="slider slider-nav-address">
          {data &&
            data.contentfulPageBanner &&
            data.contentfulPageBanner.bannerItems.map((item, key) => (
              <div className="position-relative" key={key}>
                <span className="strip-gray"></span>
                <a href="" className="text-uppercase">
                  &nbsp;
                  <i className="fa fa-map-marker"></i> {item.imageDetail}
                </a>
              </div>
            ))}
        </div>
      )}
    />
  )
}
