import React from "react"
import { Link } from "gatsby"
import { Navbar } from "./NavBar"
import { Logo } from "./Logo"
import { BannerComponent } from "./BannerComponent"

export class Header extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      sliderType: "Home",
    }
  }

  render() {
    return (
      <React.Fragment>
        <Navbar />
        <div className="header">
          <div className="container-xxl">
            <div className="row">
              <div className="container">
                <div className="row">
                  <div className="col-md-6 col-lg-4 bg-white">
                    <div className="d-flex flex-row-reverse justify-content-between flex-md-row align-items-center">
                      <a
                        href="/"
                        title="Search"
                        className="text-dark d-md-none pr-1"
                      >
                        <i className="fa fa-search"></i>
                      </a>
                      <Link to="/">
                        <Logo class="logo" />
                      </Link>
                      <span className="menu-txt menu-button ml-md-5">
                        <img
                          src="https://res.cloudinary.com/espire/image/upload/v1620205096/samples/images/Menu.svg"
                          alt="img"
                        />
                        <small className="d-none d-lg-inline-block">
                          {" "}
                          &nbsp; MENU
                        </small>
                      </span>
                    </div>
                  </div>
                  <div className="col-md-6 col-lg-8 d-none d-md-block"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* <BannerComponent /> */}
      </React.Fragment>
    )
  }
}
