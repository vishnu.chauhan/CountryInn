import React from "react"
import { ContactDetailleft } from "./ContactDetailleft"
import { BookNow } from "./BookNow"
import { BookNowMobile } from "./BookNowMobile"
import { Banner } from "./Banner"
import { SocialMedia } from "./SocialMedia"
import { BannerImageName } from "./BannerImageName"

export const BannerComponent: React.FC<any> = ({ title }) => {
  return (
    <>
      <div className="container-xxl  position-relative">
        <div className="row">
          <div className="home-page-banner">
            <div id="homeCarousel">
              <div className="main">
                <Banner />
              </div>
              <BookNow />
              <ContactDetailleft />
              <div className="d-none d-md-flex right-black-strip">
                <div className="carousel-address ml-2">
                  <BannerImageName />
                </div>
                <div>
                  <ul className="list-in d-block  sidebar-social-media">
                    <SocialMedia disaply="list-inline-items" />
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <BookNowMobile />
    </>
  )
}
