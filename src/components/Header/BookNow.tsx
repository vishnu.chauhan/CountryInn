import React from "react"
import { Link, graphql } from "gatsby"
import axios from "axios"
import { useState } from "react"

export const BookNow: React.FC<any> = ({ title }) => {
  const [action, setAction] = useState(
    "https://linkprotect.cudasvc.com/url?a=https%3a%2f%2fhotels.eglobe-solutions.com%2fcountryinn%2fBooking%2fDetail%2f12203%23bookingsteps&c=E,1,BCwiZd2pGMHWjRqJMFYgtO-Qb6W7Zpo2055YBf7JpLRTVoOjZrBgR-qE3zjofmDNFf2JQUY4cLxVyPXGWQrWQDpkcA52u5sx3-FwXmkUIBHzD6bimUDFpf4ytt5R&typo=1"
  )
  const [startDate, setStartDate] = useState(new Date())
  const [endDate, setEndDate] = useState(new Date())
  const [staydays, setStayDays] = useState(1)


  var submitFormData = () => {
    var indt = $("#datepicker-in").val()
    var outdt = $("#datepicker-out").val()
    // console.log("Stay dates...", indt, outdt)
    if (indt != "" && outdt != "") {
      var chkindt = new Date(indt.toString())
      var chkoutdt = new Date(outdt.toString())
      var timeDiff = chkoutdt.getTime() - chkindt.getTime()
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24))
      if (diffDays == 0) {
        diffDays = diffDays + 1
      }
      if (diffDays > 0) {
        setStayDays(diffDays)
      }
      window.location.href = action
    }
  }

  return (
    <div className="bottom-strip">
      <div className="container-xxl">
        <div className="row">
          <div className="col-lg-4 d-md-none d-lg-block ">
            <div className="row  fixed-hide">
              <div className="mt-2 col-12 text-right">
                <div className="mt-2 col-12 ">

                </div>
              </div>
            </div>

            <div className="row fixed-hide  mt-1 text-white">
              {/* <div className="col-5">
                <small className="click-to-flip">CLICK TO FLIP</small>
              </div> */}
              <div className="col-4">
                <span className="line d-block mt-3"></span>
              </div>
              {/* <div className="col-3 mt-2 text-right next-prev px-1">
                <span className="slideCount d-block mx-3"></span>
              </div> */}
            </div>

            <div className="d-flex fixed-show align-items-center">
              <a href="/">
                <img
                  className="logo"
                  src="https://res.cloudinary.com/espire/image/upload/v1620205095/samples/images/logo.png"
                  alt="img"
                />
              </a>
              <span className="menu-txt menu-button-fixed ml-5">
                {" "}
                <img
                  className="menu-icon"
                  src="https://res.cloudinary.com/espire/image/upload/v1620205096/samples/images/Menu.svg"
                  alt="img"
                />{" "}
                MENU
              </span>
            </div>
          </div>

          {/* <form
            onSubmit={submitFormData}
            action={action}
            method="post"
            name="frmHotelDetails"
            id="frmHotelDetails"
            className="col-lg-8 col-md-12 d-none d-md-block pr-md-0"
            target="_parent"
            autoComplete="off"
            aria-label="label"
            title="title"
          > */}
          <div className="col-lg-8 col-md-12 d-none d-md-block pr-md-0">
            <div className="row no-gutters">
              <div className="col-md-3 text-12 col-12">
                <div className="mt-2 pt-1 mx-2">
                  <span className="font-weight-500 text-body">DESTINATIONS</span>
                  <div className="bottom-input mt-1 position-relative ">
                    <img src="https://res.cloudinary.com/espire/image/upload/v1620205095/samples/images/map-icon.svg" />
                    <select
                      onChange={e => setAction(e.target.value)}
                      name="cbohotels2"
                      aria-label="label"
                      title="title"
                    >
                      <option value="">Select City</option>
                      <option value="https://hotels.eglobe-solutions.com/countryinn/Booking/Detail/12203#bookingsteps">
                        Amritsar
                      </option>
                      <option value="https://hotels.eglobe-solutions.com/countryinn/Booking/Detail/272#bookingsteps">
                        Bhimtal
                      </option>
                      <option value="https://hotels.eglobe-solutions.com/countryinn/Booking/Detail/15062#bookingsteps">
                        Budaun
                      </option>
                      <option value="https://hotels.eglobe-solutions.com/countryinn/Booking/Detail/269#bookingsteps">
                        Corbett
                      </option>

                      <option value="https://hotels.eglobe-solutions.com/countryinnvrindavan/Booking/Detail#bookingsteps">
                        Vrindavan
                      </option>
                      <option value="https://hotels.eglobe-solutions.com/countryinnharidwar/Booking/Detail#bookingsteps">
                        Haridwar
                      </option>
                    </select>
                  </div>
                </div>
              </div>

              <div className="col-md-3 text-12 col-12">
                <div className="mt-2 pt-1 mx-2">
                  <span className="font-weight-500 text-body">CHECK - IN</span>
                  <div className="bottom-input mt-1 position-relative">
                    <img src="https://res.cloudinary.com/espire/image/upload/v1620205104/samples/images/calendar-icon.svg" />
                    <input
                      //placeholder="Check In"
                      type="text"
                      name="checkInDateParam"
                      id="datepicker-in"
                      title="title"
                      aria-label="label"
                      className="date datePicker"
                      defaultValue={startDate.toLocaleDateString()}
                      value={startDate.toLocaleDateString()}
                      onChange={e => {
                        setStartDate(new Date(e.target.value))
                      }}
                    />
                  </div>
                </div>
              </div>

              <div className="col-md-3 text-12 col-12">
                <div className="mt-2 pt-1 mx-2">
                  <span className="font-weight-500 text-body">CHECK - OUT</span>
                  <div className="bottom-input mt-1 position-relative">
                    <img src="https://res.cloudinary.com/espire/image/upload/v1620205104/samples/images/calendar-icon.svg" />
                    <input
                      type="text"
                      //placeholder="Check Out"
                      title="title"
                      aria-label="label"
                      defaultValue={endDate.toLocaleDateString()}
                      name="ch_out"
                      id="datepicker-out"
                      className="date datePicker"
                      value={endDate.toLocaleDateString()}
                      onChange={e => {
                        setEndDate(new Date(e.target.value))
                      }}
                    />
                    <input
                      title="title"
                      aria-label="label"
                      type="hidden"
                      name="numNightsParam"
                      id="numNightsParam"
                      value={staydays}
                    />
                  </div>
                </div>
              </div>
              <div className="col-md-3 text-12 col-12">
                <input
                  type="submit"
                  value="Book Online"
                  className="book-now d-flex"
                  style={{
                    border: "none",
                    width: "100%",
                    background: "#9d6d28",
                    cursor: "pointer",
                  }}
                  onClick={submitFormData}
                  title="book now"
                />
              </div>
            </div>
          </div>

          {/* <div className="col-md-3 text-12 col-12">
                <div className="mt-2 pt-1 mx-2">
                  <span className="font-weight-500">DESTINATIONS</span>
                  <div className="bottom-input mt-1 position-relative ">
                    <select
                      onChange={e => setAction(e.target.value)}
                      name="cbohotels2"
                      aria-label="label"
                      title="select"
                    >
                      <option value="" aria-label="label" title="title">
                        Select City
                      </option>
                      <option value="https://linkprotect.cudasvc.com/url?a=https%3a%2f%2fhotels.eglobe-solutions.com%2fcountryinn%2fBooking%2fDetail%2f12203%23bookingsteps&c=E,1,lj1qRI8_mKdULAWaeNNGvA9u--wMH9k4Z-UxV7H3niGgXjCF7bLTi33CQ_HL_Q0Kmu2uQkSAbmvek7mxh6PAXILBg-C8KxddQq87-a7YjUy8ziI,&typo=1">
                        Amritsar
                      </option>
                      <option value="https://linkprotect.cudasvc.com/url?a=https%3a%2f%2fhotels.eglobe-solutions.com%2fcountryinn%2fBooking%2fDetail%2f272%23bookingsteps&c=E,1,knD71UFg63jtbKBpeCVzrjD0Z2SBlgWnvvVAUXpGcJ2Uzlv1Ldx1wbgrl4Hf9UF05ek9HTvRP3-bSzpAvPuPGLJauwgcnZbSH6Rw5rNDF8FFqQ,,&typo=1">
                        Bhimtal
                      </option>
                      <option value="https://linkprotect.cudasvc.com/url?a=https%3a%2f%2fhotels.eglobe-solutions.com%2fcountryinn%2fBooking%2fDetail%2f15062%23bookingsteps&c=E,1,3u_2xiJKJpsWQ8rQlXFPJendwYAcb-MNyKX-ViVT6BG-GTlloNwwsR_uNnhY_aGICgLKDLj2vl8yFITTTNfXzx37tBtT-u_O01eClopgFj6n&typo=1">
                        Budaun
                      </option>
                      <option value="https://linkprotect.cudasvc.com/url?a=https%3a%2f%2fhotels.eglobe-solutions.com%2fcountryinn%2fBooking%2fDetail%2f269%23bookingsteps&c=E,1,QgJ_wvxYxGpXyURPLCzj62d5a-H_FSA-A7v-vdZcQlfounWJnl6BfJXwx7XWL0yMH4DfBxUvWZ_rFqv3Fxnas2ddWQu9Ysy3iiLHgNsI9KCV&typo=1">
                        Corbett
                      </option>
                      <option value="https://linkprotect.cudasvc.com/url?a=https%3a%2f%2fhotels.eglobe-solutions.com%2fcountryinn%2fBooking%2fDetail%2f271%23bookingsteps&c=E,1,DzFeXOuowxGv_F5CNom21rA29DwpIwROzhIu123F19fUpY3oE4tSFDfPXmS4Uv0YMDReleRLIpNzF6gKBhsjzk2hKHrl9HInW1bzmWxV&typo=1">
                        Mussoorie
                      </option>
                      <option value="https://linkprotect.cudasvc.com/url?a=https%3a%2f%2fhotels.eglobe-solutions.com%2fcountryinnvrindavan%2fBooking%2fDetail%23bookingsteps&c=E,1,b5fzzuMeUn3IAzgHN6xosDc2XDm54pLcSwC2_ufuq_OPyHU7_zl-zNtHCf3RJpLJNo_Rljuw7PMHAIFekwUelA4FjSNIa_S1KXXqeeRCpOAFZ9NRMdU,&typo=1">
                        Vrindavan
                      </option>
                      <option value="https://linkprotect.cudasvc.com/url?a=https%3a%2f%2fhotels.eglobe-solutions.com%2fcountryinnharidwar%2fBooking%2fDetail%23bookingsteps&c=E,1,9opQqKNAZGa1jXVwCgTLt9IVYLlw_VHNBI70_PcBmgfqXDrAw3giBpo3xc2JRfaDjPQSCPcysZhHYZAtLFH9gQSqC6KLrszRYHI8gtDKED0L7dWbRIl-1YvR&typo=1">
                        Haridwar
                      </option>
                    </select>
                  </div>
                </div>
              </div>
              <div className="col-md-3 text-12 col-12">
                <div className="mt-2 pt-1 mx-2">
                  <span className="font-weight-500">CHECK - IN</span>
                  <div className="bottom-input mt-1 position-relative">
                    <img
                      src="https://res.cloudinary.com/espire/image/upload/v1620205104/samples/images/calendar-icon.svg"
                      alt="image"
                    />
                    <input
                      id="datepicker-in checkin"
                      readOnly
                      type="text"
                      placeholder="Select Date"
                      title="title"
                      aria-label="label"
                      name="checkInDateParam"
                      className="date"
                    />
                  </div>
                </div>
              </div>
              <div className="col-md-3 text-12 col-12">
                <div className="mt-2 pt-1 mx-2">
                  <span className="font-weight-500">CHECK - OUT</span>
                  <div className="bottom-input mt-1 position-relative">
                    <img
                      src="https://res.cloudinary.com/espire/image/upload/v1620205104/samples/images/calendar-icon.svg"
                      alt="image"
                    />
                    <input
                      id="datepicker-out checkout"
                      readOnly
                      type="text"
                      placeholder="Select Date"
                      name="ch_out"
                      title="title"
                      aria-label="label"
                      className="date"
                    />
                  </div>
                </div>
              </div>
              <input
                type="hidden"
                name="numNightsParam"
                id="numNightsParam"
                value="1"
                title="title"
                aria-label="label"
              />
              <div className="col-md-3 text-12 col-12">
                <button
                  className="book-now d-flex"
                  onClick={handleSubmit}
                  style={{
                    border: "none",
                    width: "100%",
                    background: "#9d6d28",
                    cursor: "pointer",
                  }}
                  title="title"
                  aria-label="label"
                >
                  BOOK
                  <span
                    className="d-none d-lg-block"
                    style={{ background: "#9d6d28" }}
                  >
                    ONLINE
                  </span>
                </button>
                <button
                  className="book-now search-icon fixed-show"
                  onClick={handleSubmit}
                  name="bootnow"
                  title="title"
                  aria-label="label"
                  style={{ border: "none", width: "100%", cursor: "pointer" }}
                >
                  <i className="fa fa-search" aria-hidden="true"></i>
                </button>
              </div>
            */}
        </div>
      </div>
    </div>
  )
}
