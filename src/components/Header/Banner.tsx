import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"
import { globalHistory } from "@reach/router"

export const Banner: React.FC<any> = props => {
  return (
    <StaticQuery
      query={graphql`
        query PageBanner {
          contentfulPageBanner(slug: { eq: "Home" }) {
            title
            slug
            bannerItems {
              subTitle
              title
              imageDetail
              description {
                description
              }
              image {
                file {
                  url
                }
              }
              mobileViewImg {
                file {
                  url
                }
              }
            }
          }
        }
      `}
      render={data => (
        <>
          <div className="slider desktop-slider slider-for-img">
            {data.contentfulPageBanner.bannerItems.map((item, key) => (
              <div className="position-relative">
                <img
                  src={`${item.image.file.url}`}
                  className="d-block w-100"
                  alt="Img"
                />
                {/* <div className="carousel-caption">
                <h3 className="tk-gautreaux">
                  {item.title}{" "}
                  <span className="  yellow-text "> {item.subTitle} </span>
                </h3>
                <p className="text-uppercase">
                  {item.description && item.description.description}
                </p>
              </div> */}
              </div>
            ))}
          </div>
           <div className="slider mobile-slider slider-for-img">
            {data.contentfulPageBanner.bannerItems.map((item, key) => (
              <div className="position-relative">
                <img
                  src={`${item.mobileViewImg.file.url}`}
                  className="d-block w-100"
                  alt="..."
                />
              </div>
            ))}
            </div>
        </>
      )}
    />
  )
}
