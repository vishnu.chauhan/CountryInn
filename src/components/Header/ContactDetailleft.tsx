import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"

export const ContactDetailleft: React.FC<any> = props => {
  return (
    <StaticQuery
      query={graphql`
        query ContactDetailleft {
          contentfulContactDetails {
            countryCode
            headerMobile
            headerEmail
          }
        }
      `}
      render={data => (
        <>
          <div className="left-black-strip d-none d-md-flex">
            <div>
              <span className="strip-gray"></span>
              <Link
                to={`mailto:${data.contentfulContactDetails &&
                  data.contentfulContactDetails.headerEmail
                  }`}
                className="text-uppercase"
              >
                &nbsp;
                <i className="fa yellow-text fa-paper-plane"></i>
                {data.contentfulContactDetails &&
                  data.contentfulContactDetails.headerEmail}
              </Link>
            </div>
            <div>
              <Link
                to={`tel:(${data.contentfulContactDetails &&
                  data.contentfulContactDetails.countryCode
                  }) ${data.contentfulContactDetails &&
                  data.contentfulContactDetails.headerMobile
                  }`}
                className="text-uppercase"
              >
                (+
                {data.contentfulContactDetails &&
                  data.contentfulContactDetails.countryCode}
                ) &nbsp;
                {data.contentfulContactDetails &&
                  data.contentfulContactDetails.headerMobile}
                &nbsp;
                <i className="fa fa-phone yellow-text"></i>
              </Link>
              <span className="strip-gray"></span>
            </div>
          </div>
        </>
      )}
    />
  )
}
