import { UniformTracker } from "@uniformdev/optimize-tracker-react"
import { localTracker } from "../lib/local-tracker"
import React from "react"
import { graphql, Link } from "gatsby"
import { Layout } from "../layouts/default"
import { PartnerWithUsHead } from "../components/PartnerWithUs/PartnerWithUsHead"
import { PartnerWithUsFormSection } from "../components/PartnerWithUs/PartnerWithUsFormSection"
import { PartnerWithUsBrandingBenefits } from "../components/PartnerWithUs/PartnerWithUsBrandingBenefits"
import { PartnerWithUsFixedText } from "../components/PartnerWithUs/PartnerWithUsFixedText"
//import { FormTest } from "../components/PartnerWithUs/FormTest"
import { Helmet } from "react-helmet"

export default class PartnerWithUs extends React.Component {
    componentDidMount() {
        $("html")
            .find(
                'script[src="https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"]'
            )
            .remove()
        const script = document.createElement("script")
        script.src =
            "https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"
        document.body.appendChild(script)
    }
    render() {

        const { pageContext, data } = this.props
        const { allContentfulPartnerWithUs } = data
        return (
            <UniformTracker trackerInstance={localTracker}>
                <Layout>
                    <Helmet
                        htmlAttributes={{
                            lang: "en-us",
                        }}
                    >
                        <meta charSet="utf-8" />
                        <title> Country inn </title>
                        <meta name="description" content="Country inn"></meta>
                        <meta name="keywords" content="Country inn"></meta>
                        <meta http-equiv="Content-Type" content="application/json"></meta>
                    </Helmet>
                    <PartnerWithUsHead allContentfulPartnerWithUs={allContentfulPartnerWithUs} />
                    <PartnerWithUsFormSection allContentfulPartnerWithUs={allContentfulPartnerWithUs} />
                    <PartnerWithUsBrandingBenefits allContentfulPartnerWithUs={allContentfulPartnerWithUs} />
                    <PartnerWithUsFixedText allContentfulPartnerWithUs={allContentfulPartnerWithUs} />
                    {/* <FormTest allContentfulPartnerWithUs={allContentfulPartnerWithUs} /> */}
                </Layout>
            </UniformTracker>
        )
    }
}

export const pageQuery = graphql`
  query partnerwithus {
    allContentfulPartnerWithUs{

        edges{
        
        node{
            descriptionpara2{

                descriptionpara2
                }
        textonlycomponentData{

                heading
                subHeading
                content
                  {
                  content
                  }
        }
        imageHead{
                file
                {
                 url
                }
        }
          heading
          subHeading
          intro
          formHeading
          formSubHeading
          subDescription
          {
          subDescription
          }
          description
          {
          description
          }
          imageHeading
          imageSubHeading
          points
          {
          bulletPoints
          }
        }
       }
      }  
     }
`