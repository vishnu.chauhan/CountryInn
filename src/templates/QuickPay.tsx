import { UniformTracker } from "@uniformdev/optimize-tracker-react"
import { localTracker } from "../lib/local-tracker"
import React from "react"
import { Layout } from "../layouts/default"
import { graphql, Link } from "gatsby"
import { BannerComponent } from "../components/Header/BannerComponent"
import { QuickPayForm } from "../components/QuickPay/QuickPayForm"
import { QuickPayHead } from "../components/QuickPay/QuickPayHead"
import { Helmet } from "react-helmet"

export default class QuickPay extends React.Component {
  render() {

    const { pageContext, data } = this.props
    const { contentfulQuickPay } = data;
    return (
      <UniformTracker trackerInstance={localTracker}>
        <Layout>
          <Helmet
            htmlAttributes={{
              lang: "en-us",
            }}
          >
            <meta charSet="utf-8" />
            <title> Country inn </title>
            <meta name="description" content="Country inn"></meta>
            <meta name="keywords" content="Country inn"></meta>
            <meta http-equiv="Content-Type" content="application/json"></meta>
          </Helmet>
          <QuickPayHead contentfulQuickPay={contentfulQuickPay} />
          <QuickPayForm contentfulQuickPay={contentfulQuickPay} />
        </Layout>
      </UniformTracker>
    )
  }
}

export const pageQuery = graphql`
  query quickPaySlug($slug: String!) {
    contentfulQuickPay(slug: { eq: $slug }) {
      formLogoBottom
      {
        file
        {
          url
        }
      }
      name
      imageQuickPay
      {
        file
        {
          url
        }
      }
    }
  }
`

