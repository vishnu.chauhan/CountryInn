import { UniformTracker } from "@uniformdev/optimize-tracker-react"
import { localTracker } from "../lib/local-tracker"
import { graphql, Link } from "gatsby"
import React from "react"
import { Layout } from "../layouts/default"
import { Helmet } from "react-helmet"
import { BannerComponent } from "../components/Header/BannerComponent"
import { PetPolicyHead } from "../components/petPolicy/petPolicyHead"
import { PetPolicyContent } from "../components/petPolicy/PetPolicyContent"
import { PetPolicyAllPolicies } from "../components/petPolicy/PetPolicyAllPolicies"
import { AboutUsStory } from "../components/AboutUs/AboutUsStory"
import { AboutUsAccordion } from "../components/AboutUs/AboutUsAccordion"

export default class petPolicy extends React.Component {
    componentDidMount() {
        $("html")
            .find(
                'script[src="https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"]'
            )
            .remove()
        const script = document.createElement("script")
        script.src =
            "https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"
        document.body.appendChild(script)
    }
    render() {

        const { pageContext, data } = this.props
        const { contentfulPetsPolicyPage } = data

        return (
            <UniformTracker trackerInstance={localTracker}>
                <Layout>
                    <Helmet
                        htmlAttributes={{
                            lang: "en-us",
                        }}
                    >
                        <meta charSet="utf-8" />
                        <title> Country inn </title>
                        <meta name="description" content="Country inn"></meta>
                        <meta name="keywords" content="Country inn"></meta>
                        <meta http-equiv="Content-Type" content="application/json"></meta>
                        <script src="https://www.google.com/recatcha/api.js"></script>
                        <script src="https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js" />
                    </Helmet>
                    <PetPolicyHead contentfulPetsPolicyPage={contentfulPetsPolicyPage} />
                    <PetPolicyContent contentfulPetsPolicyPage={contentfulPetsPolicyPage} />
                    <PetPolicyAllPolicies contentfulPetsPolicyPage={contentfulPetsPolicyPage} />
                </Layout>
            </UniformTracker>
        )
    }
}

export const pageQuery = graphql`
query petPolicyPageData {

    contentfulPetsPolicyPage{

        name
        image
        {
            file
            {
                url
            }
        }
        petImage{
        
        file
          {
        url
        }
        }
          backgroundImage{
        
          file {
        
          url
          }
          }
          petTitle
          petSubtitle
          petContentPara1
          {
          petContentPara1
          }
          petContentPara2
          {
          petContentPara2
          }
          petsPolicyList
          petWelcomePropertiesHeading
          welcomeList
        }
        
    }

`