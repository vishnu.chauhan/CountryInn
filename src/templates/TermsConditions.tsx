import { UniformTracker } from "@uniformdev/optimize-tracker-react"
import { localTracker } from "../lib/local-tracker"
import { graphql, Link } from "gatsby"
import React from "react"
import { Layout } from "../layouts/default"
import { Helmet } from "react-helmet"
import { TermsConditionHead } from "../components/TermsCondition/TermsConditionHead"
import { TermsConditionContent } from "../components/TermsCondition/TermsConditionContent"

export default class TermsConditions extends React.Component {
    componentDidMount() {
        $("html")
            .find(
                'script[src="https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"]'
            )
            .remove()
        const script = document.createElement("script")
        script.src =
            "https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"
        document.body.appendChild(script)
    }
    render() {

        const { pageContext, data } = this.props
        const { contentfulTermsConditions } = data

        return (
            <UniformTracker trackerInstance={localTracker}>
                <Layout>
                    <Helmet
                        htmlAttributes={{
                            lang: "en-us",
                        }}
                    >
                        <meta charSet="utf-8" />
                        <title> Country inn </title>
                        <meta name="description" content="Country inn"></meta>
                        <meta name="keywords" content="Country inn"></meta>
                        <meta http-equiv="Content-Type" content="application/json"></meta>
                        <script src="https://www.google.com/recatcha/api.js"></script>
                        <script src="https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js" />
                    </Helmet>
                    <TermsConditionHead contentfulTermsConditions={contentfulTermsConditions} />
                    <TermsConditionContent contentfulTermsConditions={contentfulTermsConditions} />
                </Layout>
            </UniformTracker>
        )
    }
}

export const pageQuery = graphql`
query careerTermsConditionsNew{
    contentfulTermsConditions{

          name
          title
          subtitle
          rightText
          image
          {
          file
           {
          url
          }
          }
          termAndConditionTitle
          termsAndConditionContentPara1
          {
        
          termsAndConditionContentPara1
          }
          termsAndConditionContentPara2
          {
        
          termsAndConditionContentPara2
          }
          termsAndConditionContentPara3
          {
        
          termsAndConditionContentPara3
          }
          termsAndConditionContentPara4
          {
          termsAndConditionContentPara4
          }
          privacyPolicyTitle
          privacyPolicyContentPara1
          {
        
          privacyPolicyContentPara1
          }
          privacyPolicyContentPara2
          {
        
          privacyPolicyContentPara2
          }
          refundCancellationPolicyTitle
          refundCancellationPolicyList
          childPolicyTitle
          childPolicyContent
          {
          childPolicyContent
          }
        }
    }

`