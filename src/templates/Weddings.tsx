import { UniformTracker } from "@uniformdev/optimize-tracker-react"
import { localTracker } from "../lib/local-tracker"
import { graphql, Link } from "gatsby"
import React from "react"
import { Layout } from "../layouts/default"
import { Helmet } from "react-helmet"
import { WeddingHead } from "../components/Wedding/WeddingHead"
import { WeddingContent } from "../components/Wedding/WeddingContent"
import { WeddingCities } from "../components/Wedding/WeddingCities"



export default class Weddings extends React.Component {
    componentDidMount() {
        $("html")
            .find(
                'script[src="https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"]'
            )
            .remove()
        const script = document.createElement("script")
        script.src =
            "https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"
        document.body.appendChild(script)
    }
    render() {

        const { pageContext, data } = this.props
        const { allContentfulWeddingEvents } = data

        return (
            <UniformTracker trackerInstance={localTracker}>
                <Layout>
                    <Helmet
                        htmlAttributes={{
                            lang: "en-us",
                        }}
                    >
                        <meta charSet="utf-8" />
                        <title> Country inn </title>
                        <meta name="description" content="Country inn"></meta>
                        <meta name="keywords" content="Country inn"></meta>
                        <meta http-equiv="Content-Type" content="application/json"></meta>
                        <script src="https://www.google.com/recatcha/api.js"></script>
                        <script src="https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js" />
                    </Helmet>
                    <WeddingHead allContentfulWeddingEvents={allContentfulWeddingEvents} />
                    <WeddingContent allContentfulWeddingEvents={allContentfulWeddingEvents} />
                    <WeddingCities allContentfulWeddingEvents={allContentfulWeddingEvents} />
                </Layout>
            </UniformTracker>
        )
    }
}

export const pageQuery = graphql`
query weddingEvents{
    allContentfulWeddingEvents{
        edges
        {
        
        node{
            iconImage
            {
                file
                {
                    url
                }
            }
        bannerImage
        {
            file
            {
                url
            }
        }
        backgroundImage
        {
            file
            {
                url
            }
        }
        name
        title
        description
        {
        description
        }
        cities
        {
        name
        image
        {
        file
        {
        url
        }
        }
         description
          queryLink
        }
        }
       }
      }
    }

`