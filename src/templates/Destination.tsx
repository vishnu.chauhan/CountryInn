import { UniformTracker } from "@uniformdev/optimize-tracker-react"
import { localTracker } from "../lib/local-tracker"
import React from "react"
import { graphql, Link } from "gatsby"
import { Layout } from "../layouts/default"
import { Helmet } from "react-helmet"
import { DestinationIndex } from "../components/DestinationDetail/DestinationIndex"

export default class Destination extends React.Component {
  componentDidMount() {
    $("html")
      .find(
        'script[src="https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"]'
      )
      .remove()
    const script = document.createElement("script")
    script.src =
      "https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"
    document.body.appendChild(script)
  }
  render() {
    const { pageContext, data } = this.props
    const { contentfulDestination } = data

    return (
      <UniformTracker trackerInstance={localTracker}>
        <Layout>
          <Helmet
            htmlAttributes={{
              lang: "en-us",
            }}
          >
            <meta charSet="utf-8" />
            <title>{pageContext.title}</title>
            <meta
              name="description"
              content={
                contentfulDestination &&
                contentfulDestination.scoTags &&
                contentfulDestination.scoTags.description
              }
            ></meta>
            <meta
              name="keywords"
              content={
                contentfulDestination &&
                contentfulDestination.scoTags &&
                contentfulDestination.scoTags.allMeta
              }
            ></meta>
            <meta http-equiv="Content-Type" content="application/json"></meta>
          </Helmet>
          <>
            <DestinationIndex contentfulDestination={contentfulDestination} />
          </>
        </Layout>
      </UniformTracker>
    )
  }
}

export const pageQuery = graphql`
  query destinationSlug($slug: String!) {
    contentfulDestination(slug: { eq: $slug }) {
      title
      subTitle
      slug
      scoTags {
        allMeta
        description
        title
        subtitle
      }
      address {
        address
      }

      restaurantAndFacilitiesTab {
        title
        slug
      }
      restaurantAndFacilities {
        title
        subTitle
        tableData {
          tableData
        }
        description {
          description
        }
        tag
        image {
          file {
            url
          }
        }
      }
      roomTypes {
        bookNowLink
        slug
        subTitle
        title
        heading
        description {
          description
        }
        image {
          file {
            url
            fileName
          }
        }
      }
      contactEmail

      description {
        description
      }
      facilities {
        title
        icon {
          file {
            url
          }
          title
        }
      }
      banner {
        slug
        title
        bannerItems {
          image {
            file {
              url
            }
          }
          imageDetail
          subTitle
          description {
            description
          }
          title
        }
      }
      banner {
        slug
        title
        bannerItems {
          image {
            file {
              fileName
              contentType
            }
          }
          mobileViewImg {
            file {
              url
            }
          }
        }
      }
      gallery {
        galleryPageLink
        slug
        title
        images {
          title
          file {
            fileName
            url
          }
        }
      }
    }
  }
`
