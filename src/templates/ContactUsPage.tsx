import { UniformTracker } from "@uniformdev/optimize-tracker-react"
import { localTracker } from "../lib/local-tracker"
import React from "react"
import { Layout } from "../layouts/default"
import { ContactPage } from "../components/ContactPage"
import { Helmet } from "react-helmet"
import { BannerComponent } from "../components/Header/BannerComponent"

export default class ContactUsPage extends React.Component {
  componentDidMount() {
    $("html")
      .find(
        'script[src="https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"]'
      )
      .remove()
    const script = document.createElement("script")
    script.src =
      "https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"
    document.body.appendChild(script)
  }
  render() {
    return (
      <UniformTracker trackerInstance={localTracker}>
        <Layout>
          <Helmet
            htmlAttributes={{
              lang: "en-us",
            }}
          >
            <meta charSet="utf-8" />
            <title> Country inn </title>
            <meta name="description" content="Country inn"></meta>
            <meta name="keywords" content="Country inn"></meta>
            <meta http-equiv="Content-Type" content="application/json"></meta>
            <script src="https://www.google.com/recatcha/api.js"></script>
            <script src="https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js" />
          </Helmet>
          <BannerComponent />
          <ContactPage />
        </Layout>
      </UniformTracker>
    )
  }
}
