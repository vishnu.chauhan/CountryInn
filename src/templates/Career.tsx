import { UniformTracker } from "@uniformdev/optimize-tracker-react"
import { localTracker } from "../lib/local-tracker"
import { graphql, Link } from "gatsby"
import React from "react"
import { Layout } from "../layouts/default"
import { Helmet } from "react-helmet"
import { CareerHead } from "../components/Career/CareerHead"
import { CareerPathContent } from "../components/Career/CareerPathContent"
import { CareerListContent } from "../components/Career/CareerListContent"

export default class petPolicy extends React.Component {
    componentDidMount() {
        $("html")
            .find(
                'script[src="https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"]'
            )
            .remove()
        const script = document.createElement("script")
        script.src =
            "https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"
        document.body.appendChild(script)
    }
    render() {

        const { pageContext, data } = this.props
        const { contentfulCareerPage } = data

        return (
            <UniformTracker trackerInstance={localTracker}>
                <Layout>
                    <Helmet
                        htmlAttributes={{
                            lang: "en-us",
                        }}
                    >
                        <meta charSet="utf-8" />
                        <title> Country inn </title>
                        <meta name="description" content="Country inn"></meta>
                        <meta name="keywords" content="Country inn"></meta>
                        <meta http-equiv="Content-Type" content="application/json"></meta>
                        <script src="https://www.google.com/recatcha/api.js"></script>
                        <script src="https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js" />
                    </Helmet>
                    <CareerHead contentfulCareerPage={contentfulCareerPage} />
                    <CareerPathContent contentfulCareerPage={contentfulCareerPage} />
                    <CareerListContent contentfulCareerPage={contentfulCareerPage} />
                </Layout>
            </UniformTracker>
        )
    }
}

export const pageQuery = graphql`
query careerPageData {

    contentfulCareerPage{

        name
        careerPathHeading
        imageCareerBanner{
            file{
                url
            }
        }
        bannerHeading
        bannerSubheading
        careerPathContentPara1
          {
         careerPathContentPara1
          }
          careerPathContentPara2
          {
          careerPathContentPara2
          }
          careerListHeading
          careerList
          emailToHeading
          emailid
        }

    }

`