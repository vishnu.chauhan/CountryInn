import { UniformTracker } from "@uniformdev/optimize-tracker-react"
import { localTracker } from "../lib/local-tracker"
import React from "react"
import { graphql, Link } from "gatsby"
import { Layout } from "../layouts/default"

import { Helmet } from "react-helmet"
import { DestinationGellary } from "../components/DestinationGallery"
import { BannerComponent } from "../components/Header/BannerComponent"

export default class DestinationGallary extends React.Component {
  componentDidMount() {
    $("html")
      .find(
        'script[src="https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"]'
      )
      .remove()
    const script = document.createElement("script")
    script.src =
      "https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"
    document.body.appendChild(script)
  }
  render() {
    const { pageContext, data } = this.props
    const { contentfulGallery } = data

    return (
      <UniformTracker trackerInstance={localTracker}>
        <Layout>
          <Helmet
            htmlAttributes={{
              lang: "en-us",
            }}
          >
            <meta charSet="utf-8" />
            <title>{pageContext.title}</title>
            <meta
              name="description"
              content={
                contentfulGallery &&
                contentfulGallery.scoTags &&
                contentfulGallery.scoTags.description
              }
            ></meta>
            <meta
              name="keywords"
              content={
                contentfulGallery &&
                contentfulGallery.scoTags &&
                contentfulGallery.scoTags.description
              }
            ></meta>
            <meta http-equiv="Content-Type" content="application/json"></meta>
          </Helmet>
          <BannerComponent />
          <DestinationGellary contentfulGallery={contentfulGallery} />
        </Layout>
      </UniformTracker>
    )
  }
}

export const pageQuery = graphql`
  query destinationBySlug($slug: String!) {
    contentfulGallery(slug: { eq: $slug }) {
      slug
      title
      scoTags {
        allMeta
        description
        title
        subtitle
      }
      images {
        file {
          fileName
          url
        }
        title
        description
      }
    }
  }
`
