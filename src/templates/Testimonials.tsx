import { UniformTracker } from "@uniformdev/optimize-tracker-react"
import { localTracker } from "../lib/local-tracker"
import React from "react"
import { Layout } from "../layouts/default"
import { BannerComponent } from "../components/Header/BannerComponent"

import { Helmet } from "react-helmet"

import { Testimonial } from "../components/Testimonials/Testimonial"
import { graphql } from "gatsby"
import { TestimonialsHead } from "../components/Testimonials/TestimonialsHead"

export default class Testimonials extends React.Component {



  render() {
    const { pageContext, data } = this.props
        const { allContentfulClientSays,contentfulTestimonialHead } = data
    return (
      <UniformTracker trackerInstance={localTracker}>
        <Layout>
          <Helmet
            htmlAttributes={{
              lang: "en-us",
            }}
          >
            <meta charSet="utf-8" />
            <title> Country inn </title>
            <meta name="description" content="Country inn"></meta>
            <meta name="keywords" content="Country inn"></meta>
            <meta http-equiv="Content-Type" content="application/json"></meta>
          </Helmet>
          <TestimonialsHead data={contentfulTestimonialHead} />
          <Testimonial data={allContentfulClientSays} />
        </Layout>
      </UniformTracker>
    )
  }
}
export const pageQuery = graphql`
  query TestimonialQuery {
    allContentfulClientSays {
      nodes {
        clientCity
        clientCompany
        clientName
        clientReview {
          clientReview
        }
        clientIconImg {
          id
          file {
            fileName
            url
          }
          description
        }
        clientMedia {
          description
          file {
            url
            fileName
          }
        }
      }
    }
    contentfulTestimonialHead {
      title
      image {
        file {
          url
        }
      }
      rightText
      subtitle
    }
  }
`
