import { UniformTracker } from "@uniformdev/optimize-tracker-react"
import React, { useEffect } from "react"
import { Helmet } from "react-helmet"
import { graphql } from "gatsby"

// Tracker and Component mapping to enable personalization
import { localTracker } from "../lib/local-tracker"
import { analytics } from "../lib/analytics"
import { Components, componentMapper } from "../lib/componentMapper"
// Global Layouts
import { Layout } from "../layouts/default"
import { BannerComponent } from "../components/Header/BannerComponent"

export default function ContentfulPage({ data }) {
  const { contentfulPage } = data
  useEffect(() => {
    $("html")
      .find(
        'script[src="https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"]'
      )
      .remove()
    const script = document.createElement("script")
    script.src =
      "https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"
    document.body.appendChild(script)

    if (!data) {
      return
    }
    analytics.page()
  }, [data])

  return (
    <UniformTracker
      trackerInstance={localTracker}
      componentMapping={Components}
    >
      <Helmet
        htmlAttributes={{
          lang: "en-us",
        }}
      >
        <meta charSet="utf-8" />
        <title>{`${contentfulPage.title}`}</title>
        <meta name="description" content="Country inn"></meta>
        <meta name="keywords" content="Country inn"></meta>
        <meta http-equiv="Content-Type" content="application/json"></meta>
      </Helmet>
      <Layout>
        <BannerComponent />
        {contentfulPage?.components && componentMapper(contentfulPage)}
      </Layout>
    </UniformTracker>
  )
}

// Each of the component fragments are defined in their
// individual component files
export const query = graphql`
  query PageQuery($slug: String!) {
    contentfulPage(slug: { eq: $slug }) {
      title
      slug
      components {
        ...hero
        ...experience
      }
    }
  }
`
