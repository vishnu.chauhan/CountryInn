import { UniformTracker } from "@uniformdev/optimize-tracker-react"
import { localTracker } from "../lib/local-tracker"
import React from "react"
import { graphql, Link } from "gatsby"
import { Layout } from "../layouts/default"
import { MeetingandEvents } from "../components/MeetingsEvents/MeetingandEvents"
import { MeetingandEventsCities } from "../components/MeetingsEvents/MeetingandEventsCities"
import { MeetingandEventsText } from "../components/MeetingsEvents/MeetingandEventsText"
import { MeetingsTest } from "../components/MeetingsEvents/MeetingsTest"
import { Helmet } from "react-helmet"

export default class MeetingAndEvents extends React.Component {
    componentDidMount() {
        $("html")
            .find(
                'script[src="https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"]'
            )
            .remove()
        const script = document.createElement("script")
        script.src =
            "https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"
        document.body.appendChild(script)
    }


    render() {

        const { pageContext, data } = this.props
        const { allContentfulMeetingAndEvents } = data

        return (
            <UniformTracker trackerInstance={localTracker}>
                <Layout>
                    <Helmet
                        htmlAttributes={{
                            lang: "en-us",
                        }}
                    >
                        <meta charSet="utf-8" />
                        <title> Country inn </title>
                        <meta name="description" content="Country inn"></meta>
                        <meta name="keywords" content="Country inn"></meta>
                        <meta http-equiv="Content-Type" content="application/json"></meta>
                    </Helmet>


                    <MeetingandEvents allContentfulMeetingAndEvents={allContentfulMeetingAndEvents} />
                    {/* <MeetingsTest allContentfulMeetingAndEvents={allContentfulMeetingAndEvents} /> */}
                    <MeetingandEventsText allContentfulMeetingAndEvents={allContentfulMeetingAndEvents} />
                    <MeetingandEventsCities allContentfulMeetingAndEvents={allContentfulMeetingAndEvents} />

                </Layout>
            </UniformTracker>
        )
    }
}

export const pageQuery = graphql`
query newmeetingandevents {
    allContentfulMeetingAndEvents{
    
        edges{
        
        node{
            title
            subtitle
            image{
                file{
                    url
                }
            }
        
            shortDescription{
               

                shortDescription}
                  shortDescriptionLineSecond
                  aboveDescription{
        
                  aboveDescription}
                  description{
        
                  description}
                  intro{
        
                  intro
                  }    
        cities{
        
        name
          description
          slug
          image{
        
          file{
        
          url
          }}
        }
    }
    }
    }
    }
`