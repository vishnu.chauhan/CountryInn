import { UniformTracker } from "@uniformdev/optimize-tracker-react"
import { localTracker } from "../lib/local-tracker"
import React from "react"
import { Layout } from "../layouts/default"
import { BannerComponent } from "../components/Header/BannerComponent"

import { Helmet } from "react-helmet"

export default class Packages extends React.Component {
  render() {
    return (
      <UniformTracker trackerInstance={localTracker}>
        <Layout>
          <Helmet
            htmlAttributes={{
              lang: "en-us",
            }}
          >
            <meta charSet="utf-8" />
            <title> Country inn </title>
            <meta name="description" content="Country inn"></meta>
            <meta name="keywords" content="Country inn"></meta>
            <meta http-equiv="Content-Type" content="application/json"></meta>
          </Helmet>
          <BannerComponent />
          <h1 style={{ color: "#cccc" }}> Package page Comming soon </h1>
        </Layout>
      </UniformTracker>
    )
  }
}
