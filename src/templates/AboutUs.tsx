import { UniformTracker } from "@uniformdev/optimize-tracker-react"
import { localTracker } from "../lib/local-tracker"
import { graphql, Link } from "gatsby"
import React from "react"
import { Layout } from "../layouts/default"
import { Helmet } from "react-helmet"
import { BannerComponent } from "../components/Header/BannerComponent"
import { AboutUsHead } from "../components/AboutUs/AboutUsHead"
import { AboutUsStory } from "../components/AboutUs/AboutUsStory"
import { AboutUsAccordion } from "../components/AboutUs/AboutUsAccordion"

export default class AboutUs extends React.Component {
    componentDidMount() {
        $("html")
            .find(
                'script[src="https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"]'
            )
            .remove()
        const script = document.createElement("script")
        script.src =
            "https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js"
        document.body.appendChild(script)
    }
    render() {

        const { pageContext, data } = this.props
        const { contentfulAboutUsPage } = data

        return (
            <UniformTracker trackerInstance={localTracker}>
                <Layout>
                    <Helmet
                        htmlAttributes={{
                            lang: "en-us",
                        }}
                    >
                        <meta charSet="utf-8" />
                        <title> Country inn </title>
                        <meta name="description" content="Country inn"></meta>
                        <meta name="keywords" content="Country inn"></meta>
                        <meta http-equiv="Content-Type" content="application/json"></meta>
                        <script src="https://www.google.com/recatcha/api.js"></script>
                        <script src="https://res.cloudinary.com/espire/raw/upload/v1621745582/samples/Js/custom.js" />
                    </Helmet>
                    <AboutUsHead contentfulAboutUsPage={contentfulAboutUsPage} />
                    <AboutUsStory contentfulAboutUsPage={contentfulAboutUsPage} />
                    <AboutUsAccordion contentfulAboutUsPage={contentfulAboutUsPage} />
                </Layout>
            </UniformTracker>
        )
    }
}

export const pageQuery = graphql`
query AboutUsPageData {
    

        contentfulAboutUsPage{
            arrayImage
            {
            file
            {
            url
            }
            }
            image
            {
                file
                {
                    url
                }
            }
          storyPara1
          {
          storyPara1
          }
          storyPara2
  {
  storyPara2
  }
  storyPara3
  {
  storyPara3
  }
  storyPara4
  {
  storyPara4
  }
  storyPara5
  {
  storyPara5
  }
          experienceTabContent
          {
          experienceTabContent
          }
          affilicationsList
          ecofriendlyParaContent
          {
          ecofriendlyParaContent
          }
          ecofriendlyListHeading
          {
          ecofriendlyListHeading
          }
          ecofriendlyList
          ecofriendlyListHeading2
          ecofriendlyProductlist
          
        }
        }
       
`
