import { UniformTracker } from "@uniformdev/optimize-tracker-react"
import { localTracker } from "../lib/local-tracker"
import React from "react"
import { Layout } from "../layouts/default"
import { BannerComponent } from "../components/Header/BannerComponent"

import { Helmet } from "react-helmet"
import { TermsAndCondition } from "../components/Footer/TermsAndCondition"

export default class TermsAndConditions extends React.Component {
  render() {
    return (
      <UniformTracker trackerInstance={localTracker}>
        <Layout>
          <Helmet
            htmlAttributes={{
              lang: "en-us",
            }}
          >
            <meta charSet="utf-8" />
            <title> Country inn </title>
            <meta name="description" content="Country inn"></meta>
            <meta name="keywords" content="Country inn"></meta>
            <meta http-equiv="Content-Type" content="application/json"></meta>
          </Helmet>
          <BannerComponent />
          <TermsAndCondition/>
        </Layout>
      </UniformTracker>
    )
  }
}
