import React from "react"
import { Header } from "../components/Header"
import { Footer } from "../components/Footer"

export const Layout = ({ children }) => {
  return (
    <>
      <header>
        <Header />
      </header>
      <main className="leading-normal tracking-normal text-white gradient">
        {children}
      </main>
      <Footer />
    </>
  )
}
